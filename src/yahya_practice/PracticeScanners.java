package yahya_practice;

import java.util.Scanner;

public class PracticeScanners {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String fName, lName, address;
        int n1,n2,n3;

        System.out.println("First Name: ");
        fName = input.next();

        System.out.println("Last Name: ");
        lName = input.next();
        input.nextLine();

        System.out.println("Address");
        address = input.nextLine();

        System.out.println("Favourite number");
        n1 = input.nextInt();

        System.out.println("Age");
        n2 = input.nextInt();

        System.out.println("year born");
        n3 = input.nextInt();

        System.out.println("First: " + fName + " " + lName + "\n" + "Address: " + address + "\n" + "Favourite number: " + n1
        + "\n" + "Age: " + n2 + "\n" + "year born: " + n3);



    }
}
