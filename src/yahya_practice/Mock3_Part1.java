package yahya_practice;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;

public class Mock3_Part1 {
    public static void main(String[] args) {
        System.out.println(Arrays.toString(noDuplicates(new int[]{5, 5, 2, 1, 4, 4, 2, 1})));

        System.out.println(dupiii(new ArrayList<>(Arrays.asList(5,5,2,1,4,4,2,1,7,8))));
    }

    public static int[] noDuplicates(int[] arr) {
        ArrayList<Integer> list = new ArrayList<>();

        for (int n : arr) {
            if (!list.contains(n)) list.add(n);
        }
        int[] result = new int[list.size()];
        for (int i = 0; i < list.size(); i++) {
            result[i] = list.get(i);
        }
        return result;
    }

    public static int dupiii (ArrayList<Integer> list){
        int count =0;
        HashSet<Integer> list2 = new HashSet<>();
        for (int s:list){
            if (!list2.add(s))
                count++;

        }
           return count;

    }
}
