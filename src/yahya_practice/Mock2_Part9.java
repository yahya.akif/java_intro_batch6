package yahya_practice;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class Mock2_Part9 {
    public static void main(String[] args) {
        ArrayList <Integer> list = new ArrayList<Integer>(Arrays.asList(2,3,7,1,1,7,1));
        System.out.println(secondMax(list));

        ArrayList <Integer> list2 = new ArrayList<Integer>(Arrays.asList(5,7,2,2,10,10));
        System.out.println (secondMin(list2));

        ArrayList<String> list00 = new ArrayList<String>(Arrays.asList("Tech", "Global", "", null, "", "School"));
        System.out.println(removeEmpty(list00));

    }

    public static int secondMax (ArrayList<Integer> list) {
        int max = 0;
        int second =0;
        int x;
        for (Integer integer : list) {
            if (integer > max) {
                second = max;
                max = integer;
            }

        }
        return second;
    }

    public static int secondMin (ArrayList<Integer> list2) {
        int min = 1000;
        int secondmin = 1000;

        for (int i = 0; i < list2.size(); i++) {
            if (list2.get(i) < min) {
                secondmin = min;
                min = list2.get(i);
            }

        }

        return secondmin;
    }
    public static String removeEmpty (ArrayList <String> list5) {

        for (int i = 0; i < list5.size(); i++) {

            list5.removeIf(element -> element == null || element.isEmpty());

        }
        return list5.toString();
    }


}
