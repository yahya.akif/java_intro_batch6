package yahya_practice;

import java.util.Random;
import java.util.Scanner;

public class Mock2_Part7 {
    public static void main(String[] args) {
//        System.out.println("\n-------task1---------\n");
//
//        Random r = new Random();
//        int num = r.nextInt(10)+1;
//        System.out.println(num);
//        if (num%2 == 0 && num%5 == 0) System.out.println("FooBar");
//        else if (num%2 == 0) System.out.println("Foo");
//        else if (num%5 == 0) System.out.println("Bar");
//        else System.out.println(num);
//
//        System.out.println("\n-------task2---------\n");
//
        Scanner scanner = new Scanner(System.in);
//        int s = 0;
//
//        do {
//           s = scanner.nextInt();
//        } while (s < 10);
//        System.out.println("\n-------task3---------\n");
//
//        int n = scanner.nextInt();
//        int n2 = scanner.nextInt();
//
//        for (int i = Math.min(n,n2); i <= Math.max(n,n2) ; i++) {
//            if (i == 5) continue;
//            System.out.println(i);
//        }
//        System.out.println("\n-------task4---------\n");

        //skip
        System.out.println("\n-------task5---------\n");

        int number = scanner.nextInt();

        int first = 0;
        int second = 1;
        int nextNum;

        System.out.print(first + " - " + second);
        for (int i = 2; i < number; i++) {
            nextNum = first + second;
            first = second;
            second = nextNum;

            System.out.print(" - " + nextNum);
        }

        System.out.println("\n-------task6---------\n");

        int n = 0;
        int sum = 0;
        boolean isFirst = true;
        boolean zebee = false;

        do {
            System.out.println("Please enter a number");
             n = scanner.nextInt();
            sum += n;
            System.out.println(sum);
            if (n >= 100 && isFirst) {
                System.out.println("The number is already more than 100");
                zebee = true;
            }
            isFirst = false;
        } while (sum < 100);
        if (!zebee) {
            System.out.println("Sum of entered number is at lest 100");
        }
    }
}
