package yahya_practice;

import java.util.ArrayList;
import java.util.Arrays;

public class Mock2_Part1 {
    public static void main(String[] args) {

        //Palindrome check for the word "civic"

        String word = "civic";
        String revWord = "";

        for (int i = word.length()-1; i >= 0; i--) {
            revWord += word.charAt(i);
        }

        System.out.println(word.equals(revWord));

        System.out.println("\n------2nd example Fibonachiii");

        // 0 - 1 - 1 - 2 - 3 - 5 - 8 - 13

//        String answer = "";
//        int fib = 10;
//
//        int first = 0;
//        int second = 1;
//
//        for (int i = 0; i < fib; i++) {
//            answer += first + " ";
//            int nextNum = first + second;
//            first = second;
//            second = nextNum;
//        }
//        System.out.println(answer);

        ArrayList<Integer> answer = new ArrayList<>();
        int fib = 10;
        int first = 0;
        int second = 1;

        for (int i = 0; i < fib; i++) {
            answer.add(first);
            int nextNum = first + second;
            first = second;
            second = nextNum;
        }
        System.out.println(answer);

        System.out.println("\n----------switch first and last-------\n");

        int[] arr = {0,1,2,3,4,5,6};

        System.out.println(Arrays.toString(arr));

        int firstNum = arr[0];
        arr[0] =arr[arr.length-1];
        arr[arr.length-1] = firstNum;

        System.out.println(Arrays.toString(arr));


    }
}
