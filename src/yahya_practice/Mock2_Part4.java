package yahya_practice;

import java.util.Random;

public class Mock2_Part4 {
    public static void main(String[] args) {

        Random r = new Random ();
        int num = r.nextInt(50);

        System.out.println(num);
        System.out.println(num * 5);

        System.out.println("\n-------task2---------\n");

        int n = r.nextInt(10)+1;
        int n2 = r.nextInt(10)+1;

        System.out.println(n);
        System.out.println(n2);
        System.out.println("Min number = " + Math.min(n,n2));
        System.out.println("Max number = " + Math.max(n,n2));
        System.out.println("Difference = " + Math.abs(n-n2));

        System.out.println("\n--------task3--------\n");

        int num2 = r.nextInt(51)+50;
        System.out.println(num2);
        System.out.println(num2%10);
        System.out.println("\n--------task4--------\n");

        int m1 = r.nextInt(10)+1;
        int m2 = r.nextInt(10)+1;
        int m3 = r.nextInt(10)+1;
        int m4 = r.nextInt(10)+1;
        int m5 = r.nextInt(10)+1;

        System.out.println(m1);
        System.out.println(m2);
        System.out.println(m3);
        System.out.println(m4);
        System.out.println(m5);
        System.out.println(m1*5 + m2*4 + m3*3 + m4*2 + m5);
        System.out.println("\n--------task5--------\n");

        int x = r.nextInt(25)+1;
        int x2 = r.nextInt(25)+26;
        int x3 = r.nextInt(25)+51;
        int x4 = r.nextInt(25)+76;
        int max = Math.max(x3,x4);
        int min = Math.min(x,x2);
        System.out.println("Difference of max and min = " + Math.abs(min-max));
        System.out.println("Difference of second and third = " + Math.abs(x2-x3));
        System.out.println("Average of all =" + ((x+x2+x3+x4)/4));
        System.out.println("\n--------done--------\n");
    }
}
