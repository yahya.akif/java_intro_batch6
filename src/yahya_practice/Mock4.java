package yahya_practice;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class Mock4 {
    public static void main(String[] args) {
        System.out.println(mapCount(new String[]{"Apple", "Apple", "Orange", "Apple","Kiwi", "Kiwi"}));
    }
    /*
    string array
    return map
       */

    public static Map<String, Integer> mapCount(String [] arr){
        Map<String,Integer> fruits = new LinkedHashMap<>();

        for (String e:arr) {
            fruits.put(e, fruits.getOrDefault(e,0)+1);
        }
    return fruits;
    }
    }





