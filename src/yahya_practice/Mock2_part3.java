package yahya_practice;

import utitlities.ScannerHelper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class Mock2_part3 {
    public static void main(String[] args) {
        int num = ScannerHelper.getNumber();
        System.out.println(Arrays.toString(fib(num)));

        System.out.println("\n--------Palindrome--------\n");

        String x = ScannerHelper.getString();
        System.out.println(reverseString(x));

        System.out.println("\n------Array []----------\n");


        String[] list = {"TechGlobal", "Batch6", "TechGlobal", "JohnDoe"};
        System.out.println(Arrays.toString(removeDup(list)));
    }
    public static int [] fib (int arr) {

        int [] fibooo = new int[arr];
       int first = 0;
       int second = 1;
        fibooo [0] = first;
        fibooo [1] = second;
        for (int i = 2; i <arr; i++) {
            int nextnum = first + second;
            first = second;
            second = nextnum;
            fibooo [i] = nextnum;

        }
        return fibooo;
    }

    // reverse a String

    public static String reverseString (String str){
        String reverseS = "";

        for (int i = str.length()-1; i >= 0; i--) {
            reverseS += str.charAt(i);
        }
             if (str.contains(reverseS))
                 return str + " is a Palindrome";
            else return "It is not a palindrome";

    }

    public static String [] removeDup (String [] arr) {
        ArrayList <String> duplicates = new ArrayList <>();
        for (String element: arr) {
            if (!duplicates.contains(element)) duplicates.add(element);

        }
        return duplicates.toArray(new String[0]);
    }

}
