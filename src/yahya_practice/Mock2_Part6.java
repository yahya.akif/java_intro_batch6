package yahya_practice;

import java.util.Scanner;

public class Mock2_Part6 {
    public static void main(String[] args) {
        System.out.println("\n-------task1---------\n");
        Scanner s = new Scanner(System.in);

//        System.out.println("Please enter you input");
//        String str = s.nextLine();
//
//        System.out.println(str);
//
//        if (str.length() > 0) { System.out.println(str.length());
//            System.out.println(str.charAt(0));
//            System.out.println(str.charAt(str.length()-1));
//
//        } else System.out.println("Length is zero");
//        System.out.println("\n-------task2---------\n");
//        System.out.println("Please enter you input");
//        String str2 = s.nextLine();
//        if (str2.length() > 2) {
//            if (str2.length()%2 == 1) System.out.println(str2.substring(str2.length()/2,str2.length()/2+1));
//            else System.out.println(str2.substring(str2.length()/2-1,str2.length()/2+1));
//        }
//        else System.out.println("Length is less than 3");
//        System.out.println("\n-------task3---------\n");
//
//        System.out.println("Please enter new string");
//        String str3 = s.nextLine();
//
//        if (str3.length() > 3) {
//            System.out.println("First two characters = " + str3.substring(0, 2));
//            System.out.println("Last two characters = " + str3.substring(str3.length() - 2));
//            System.out.println("other characters = " + str3.substring(2, str3.length() - 2));
//        }else System.out.println("Invalid Input");
//
//        System.out.println("\n-------task4---------\n");
//        System.out.println("Please enter you input");
//        String str4 = s.nextLine();
//        if (str4.length() >= 2) {
//            if (str4.substring(0, 2).equals(str4.substring(str4.length() - 2))) System.out.println(true);
//            else System.out.println(false);
//        } else System.out.println("The length is less than 2");
//        System.out.println("\n-------task5---------\n");
//        String x = s.nextLine ();
//        String y = s.nextLine ();
//
//        if (x.length() >= 2 && y.length() >=2) System.out.println(x.substring(1,x.length()-1) + y.substring(1,y.length()-1));
//        else System.out.println("INVALID Input");
        System.out.println("\n-------task6---------\n");
        String z = s.nextLine ();

        if (z.length () >3) {
            if (z.startsWith("xx") && z.endsWith("xx")) System.out.println(true);
            else System.out.println(false);
        }

        else System.out.println("Invalid input");
    }
}
