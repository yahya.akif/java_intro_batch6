package projects;

import com.sun.xml.internal.ws.api.model.wsdl.WSDLOutput;

import java.util.ArrayList;
import java.util.Arrays;

public class Project06 {
    public static void main(String[] args) {
        System.out.println("\n---------Task-1-------\n");
        int[] task1 = {10, 7, 7, 10, -3, 10, -3};
        findGreatestAndSmallestWithSort(task1);

        System.out.println("\n---------Task-2--------\n");
        int[] task2 = {10, 7, 7, 10, -3, 10, -3};
        findGreatestAndSmallestWithoutSort(task2);

        System.out.println("\n---------Task-3-------\n");
        int [] task3 = {10, 5, 6, 7, 8, 5, 15, 15};
        findSecondGreatestAndSmallestWithSort(task3);

        System.out.println("\n---------Task-4-------\n");
        int [] task4 = {10, 5, 6, 7, 8, 5, 15, 15};
        findSecondGreatestAndSmallest(task4);

        System.out.println("\n---------Task-5-------\n");
        String [] task5 = {"foo", "bar", "Foo", "bar", "6", "abc", "6", "xyz"};
        findDuplicatedElements(task5);

        System.out.println("\n---------Task-6-------\n");
        String [] task6 = {"pen", "eraser", "pencil", "pen", "123", "abc", "pen", "eraser"};
        findMostRepeatedElementIn(task6);

    }

    public static void findGreatestAndSmallestWithSort(int[] arr) {

        if (arr.length > 0) {
            Arrays.sort(arr);
            System.out.println("Smallest =" + arr[0]);
            System.out.println("Greatest =" + arr[arr.length - 1]);
        } else System.out.println("The Array contains no data");
    }

    public static void findGreatestAndSmallestWithoutSort(int[] nums) {

        if (nums.length > 0) {
            int greatest = nums[0];
            int smallest = nums[0];
            for (int num : nums) {
                if (greatest < num) greatest = num;
                else if (smallest > num)  smallest = num;
                }
                System.out.println("Smallest = " + smallest);
                System.out.println("Greatest = " + greatest);


            } else System.out.println("The Array contains no data");
        }

    public static void  findSecondGreatestAndSmallestWithSort (int [] nums) {

        if (nums.length > 0) {
            Arrays.sort(nums);
            int minimum = nums[0];
            int maximum = nums[nums.length-1];

            int secondMin = 0;
            int secondMax = 0;

            for (int i = 0; i < nums.length; i++) {
                if (nums[i] > minimum) {
                    secondMin = nums[i];
                    break;
                }
            }

            for (int i = nums.length-1 ; i > 0; i--) {
                if (nums[i] < maximum) {
                    secondMax = nums[i];
                    break;
                }

        }
            System.out.println("Second Smallest = " + secondMin);
            System.out.println("Second Greatest = " + secondMax);

        }else System.out.println("The array is empty");
    }

    public static void findSecondGreatestAndSmallest (int [] nums) {

        if (nums.length > 0) {
            int minimum = Integer.MAX_VALUE;
            int maximum = Integer.MIN_VALUE;
            int secondMinimum = Integer.MAX_VALUE;
            int secondMaximum = Integer.MIN_VALUE;
            for (int num : nums) {

//           System.out.println("nuum:  "+num);
//              System.out.println("minimum: "+ minimum);
//              System.out.println("secondmin: "+ secondMinimum);
//              System.out.println("                           ");

                if (num > secondMaximum && num < maximum) {
                    secondMaximum = num;
                }
                if (num > maximum) {
                    secondMaximum = maximum;
                    maximum = num;
                }

                if (num < secondMinimum && num > minimum) {
                    secondMinimum = num;
                }
                if (num < minimum) {
                    secondMinimum = minimum;
                    minimum = num;
                }



//                for (int i = nums.length - 1; i > 0; i--) {
//                    if (nums[i] != maximum && nums[i] < maximum)
//                        nums[i] = secondMaximum;
//                        break;
//                    }
//
//                if (num < minimum) num = minimum;
//                for (int i = 0; i < nums.length; i++) {
//                    if (nums[i] != minimum && nums[i] > minimum)
//                        nums[i] = secondMinimum;
//                    break;
//                }

            }
            System.out.println("Second Smallest = " + secondMinimum);
            System.out.println("Second Greatest = " + secondMaximum);

        } else System.out.println("The array is empty");
    }

    public static void findDuplicatedElements (String [] elements) {
        ArrayList<String> list1 = new ArrayList<>();
        ArrayList<String> sol = new ArrayList<>();

        for (String element:elements) {
            if (!list1.contains(element)) {
                list1.add(element);
            } else {
                sol.add(element);
            }
        }
        for (String element:sol) {
            System.out.println(element);
        }

    }

    public static void findMostRepeatedElementIn (String [] elements) {
        ArrayList<String> word = new ArrayList<>();
        ArrayList<Integer> num = new ArrayList<>();
        int maxCount = 0;
        String result = "";

        for (String element:elements) {
            if (!word.contains(element)) {
                word.add(element);
                num.add(1);
            } else {
                num.set(word.indexOf(element), num.get(word.indexOf(element)) + 1);
                if (num.get(word.indexOf(element)) > maxCount) {
                    maxCount = num.get(word.indexOf(element));
                    result = element;
                }
            }
        }
        System.out.println(result);
    }
}





