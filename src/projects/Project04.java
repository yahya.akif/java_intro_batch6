package projects;

import utitlities.ScannerHelper;

public class Project04 {
    public static void main(String[] args) {
        System.out.println("\n============Task-1=============");

        String str = ScannerHelper.getString();

        if (str.length() <= 8) {
            System.out.println("This String does not have 8 characters");
        } else {
            System.out.println(str.substring(str.length() - 4) + str.substring(4,str.length()-4) + str.substring(0, 4));
        }

        System.out.println("\n============Task-2=============");

        String str9 = ScannerHelper.getString();

       if (str9.contains(" ")) {
           System.out.println(str9.substring(str9.lastIndexOf(' ')+1) + str9.substring(str9.indexOf(' '),str9.lastIndexOf(' ')) + " " + str9.substring(0,str9.indexOf(' ')));
      } else { System.out.println("The sentence does not have two or more words to swap");

       }


        System.out.println("\n============Task-3=============");

       String str3 = ScannerHelper.getString();

        System.out.println(str3.replace("idiot","nice").replace("stupid","nice"));

        System.out.println("\n============Task-4=============");

        String str4 = ScannerHelper.getString();

        if (str4.length() <2) {
            System.out.println("Invalid input!!");
        } else {
            if (str4.length() % 2 != 0 ) {
                System.out.println(str4.substring(str4.length()/2,str4.length()/2+1));
            } else {
                System.out.println(str4.substring(str4.length() / 2 - 1,str4.length() / 2 + 1));
            }
        }

        System.out.println("\n============Task-5=============");

        String country = ScannerHelper.getString();

        if (country.length() <= 5) {
            System.out.println("Invalid input!!");
        } else {
            System.out.println(country.substring(2, country.length() - 2));
        }

        System.out.println("\n============Task-6=============");

        String address = ScannerHelper.getString();

        System.out.println(address.toLowerCase().replace("a","*").replace("e","#")
                .replace("i","+").replace("o","@").replace("u","$"));

        System.out.println("\n============End-of-Program=============");
    }
}
