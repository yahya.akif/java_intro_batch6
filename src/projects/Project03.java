package projects;


import java.util.Random;


public class Project03 {
    public static void main(String[] args) {

        System.out.println("==============Task-1=============");

        String s1 = "24";
        String s2 = "5";

        int n1 = Integer.parseInt(s1);
        int n2 = Integer.parseInt(s2);

        System.out.println("The sum of " + n1 + " and " + n2 + " = " + (n1 + n2));
        System.out.println("The subtraction of " + n1 + " and " + n2 + " = " + (n1 - n2));
        System.out.println("The division of " + n1 + " and " + n2 + " = " + ((double) n1 / n2));
        System.out.println("The multiplication of " + n1 + " and " + n2 + " = " + (n1 * n2));
        System.out.println("The remainder of " + n1 + " and " + n2 + " = " + (n1 % n2));

        System.out.println("==============Task-2=============");
        Random r = new Random();
        int num1 = r.nextInt(35) + 1;

        System.out.println(r);
        System.out.println(num1);

        if (num1 == 2 || num1 == 3 || num1 == 5 || num1 == 7 || num1 == 11 || num1 == 13 || num1 == 17 || num1 == 19 ||
                num1 == 23 || num1 == 29 || num1 == 31) {
            System.out.println(num1 + " IS A PRIME NUMBER");
        } else {
            System.out.println(num1 + " IS NOT A PRIME NUMBER");
        }

        System.out.println("==============Task-3=============");

        int num2 = r.nextInt(50) + 1;
        int num3 = r.nextInt(50) + 1;
        int num4 = r.nextInt(50) + 1;


        System.out.println("Random number generated = " + num2);
        System.out.println("Random number generated = " + num3);
        System.out.println("Random number generated = " + num4);

        if (num2 < num3 && num2 < num4 && num3 < num4) {
            System.out.println("Lowest number is =" + num2);
            System.out.println("Middle number is = " + num3);
            System.out.println("Greatest number is = " + num4);
        } else if (num2 < num3 && num2 < num4 && num4 < num3) {
            System.out.println("Lowest number is =" + num2);
            System.out.println("Middle number is = " + num4);
            System.out.println("Greatest number is = " + num3);
        } else if (num3 < num2 && num3 < num4 && num2 < num4) {
            System.out.println("Lowest number is =" + num3);
            System.out.println("Middle number is = " + num2);
            System.out.println("Greatest number is = " + num4);
        } else if (num3 < num2 && num3 < num4 && num4 < num2) {
            System.out.println("Lowest number is =" + num3);
            System.out.println("Middle number is = " + num4);
            System.out.println("Greatest number is = " + num2);
        } else if (num4 < num2 && num4 < num3 && num2 < num3) {
            System.out.println("Lowest number is =" + num4);
            System.out.println("Middle number is = " + num2);
            System.out.println("Greatest number is = " + num3);
        } else {
            System.out.println("Lowest number is =" + num4);
            System.out.println("Middle number is = " + num3);
            System.out.println("Greatest number is = " + num2);
        }


        System.out.println("==============Task-4=============");

        System.out.println("Please enter a single character");

        char c1 = 'v';

        System.out.println(c1);

        if (c1 >= 65 && c1 <= 90) {
            System.out.println("The letter is uppercase");
        } else if (c1 >= 97 && c1 <= 122) {
            System.out.println("The letter is lowercase");
        } else {
            System.out.println("Invalid character detected!!!");

        }
        System.out.println("==============Task-5=============");

        char b1 = 'a';


        if (b1 < 65 || b1 > 90 && b1 < 97 || b1 > 122) {
            System.out.println("Invalid character detected!");
        } else if (b1 == 'a' || b1 == 'e' || b1 == 'i' || b1 == 'o' || b1 == 'u' || b1 == 'A' || b1 == 'E' || b1 == 'I' || b1 == 'O' || b1 == 'U') {
            System.out.println("The letter is vowel");
        } else {
            System.out.println("The letter is constant");
        }

        System.out.println("==============Task-6=============");

        char z1 = 's';

        if (z1 < 65 || z1 > 90 && z1 < 97 || z1 > 122) {
            System.out.println("Special character = " + z1);
        } else {System.out.println("Invalid character detected!!!");

        }
        System.out.println("==============Task-7=============");

        char x1 = 'D';

        if (x1 > 47 && x1 < 58) {
            System.out.println("Character is a digit");
        } else if (x1 < 65 || x1 > 90 && x1 < 97 || x1 > 122) {
            System.out.println("Character is a special character!!!");
        } else {
            System.out.println("Character is a letter" );
            
        }


    }
    }