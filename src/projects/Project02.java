package projects;

import java.util.Scanner;

public class Project02 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("=============Task-1============");
        int number1, number2, number3;

        System.out.println("Please enter number 1");
        number1 = scanner.nextInt();

        System.out.println("Please enter number 2");
        number2 = scanner.nextInt();

        System.out.println("Please enter number 3");
        number3 = scanner.nextInt();

        System.out.println("The product of the numbers entered is = " + (number1 * number2 * number3));

        System.out.println("=============Task-2============");

        String fName, lName;
        int yearOfBirth;

        System.out.println("Please enter your first Name? ");
        fName = scanner.next();
        System.out.println("Please enter your last Name? ");
        lName = scanner.next();
        System.out.println("Please enter the year you were born? ");
        yearOfBirth = scanner.nextInt();
        scanner.nextLine();

        System.out.println(fName + " " + lName + "'s age is = " + (2023-yearOfBirth));

        System.out.println("=============Task-3============");
        String fullName;
        int weight;

        System.out.println("Please enter you full name? ");
        fullName = scanner.nextLine();

        System.out.println("Please enter your weight?");
        weight = scanner.nextInt();
        scanner.nextLine();

        System.out.println( fullName + "'s weight is = " + (double) weight*2.205 + " lbs.");

        System.out.println("=============Task-4============");
        String student1, student2, student3;
        int age1, age2, age3, age4, age5;

        System.out.println("Please enter your full name? ");
        student1 = scanner.nextLine();

        System.out.println("Please enter your age? ");
        age1 = scanner.nextInt();
        scanner.nextLine();

        System.out.println("Please enter your full name? ");
        student2 = scanner.nextLine();

        System.out.println("Please enter your age? ");
        age2 = scanner.nextInt();
        scanner.nextLine();

        System.out.println("Please enter your full name? ");
        student3 = scanner.nextLine();

        System.out.println("Please enter your age? ");
        age3 = scanner.nextInt();

        age4 = Math.max(age1,age2);

        age5 = Math.min(age1,age2);

        System.out.println(student1 + "'s age is = " + age1 + ".\n" + student2 + "'s age is = " + age2 + ".\n" +
                student3 + "'s age is = " + age3 + ".\n The average is = " + ((age1+age2+age3)/3) + ".\n The eldest age is = "
                + Math.max(age4,age3) + ".\n The youngest age is = " + Math.min(age5,age3) + ".");
    }
}
