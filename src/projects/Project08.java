package projects;


import java.util.ArrayList;
import java.util.Arrays;

public class Project08 {
    public static void main(String[] args) {
        System.out.println("-------------Task-1-------------");
        int[] numbers = {4, 8, 7, 15};
        System.out.println(findClosest(numbers));
        int[] numbers2 = {10, -5, 20, 50, 100};
        System.out.println(findClosest(numbers2));
        int[] numbers3 = {4};
        System.out.println(findClosest(numbers3));

        System.out.println("-------------Task-2-------------");

        int[] num = {2};
        System.out.println(findSingle(num));
        int[] num2 = {5, 3, -1, 3, 5, 7, -1};
        System.out.println(findSingle(num2));

        System.out.println("-------------Task-3-------------");

        System.out.println(firstUnique(new String("Hello")));

        System.out.println("-------------Task-4-------------");

        int [] arr = {2,4};
        System.out.println(findMissing(arr));

        int [] arr2 = {4,7,8,6};
        System.out.println(findMissing(arr2));
        System.out.println("--------Task-End-of-Program-----");
    }

    public static int findClosest(int[] arr) {
        int difference;
        int closest = Integer.MAX_VALUE;
        if (arr.length > 1) {
            for (int i = 0; i < arr.length - 1; i++) {
//                System.out.println("l1");
                for (int j = i + 1; j < arr.length; j++) {
                    difference = Math.abs(arr[j] - arr[i]);

                    closest = Math.min(difference, closest);
//                    System.out.println(closest);  check for numbers out
//                    System.out.println(difference); check for difference
//                    System.out.println("                ");
                }
            }
        } else return -1;
        return closest;
    }

    public static int findSingle(int[] arr) {
        int j = 0;
        for (int i = 0; i < arr.length - 1; i++) {
            if (arr[i] != arr[i + 1]) {
                arr[j] = arr[i];

                //System.out.println(j);
            }
        }
        return arr[j];
    }

    public static char firstUnique(String str) {
        int count = 0;
        for (int i = 0; i < str.length() - 1; i++) {
           char c = str.charAt(i);
                if (str.indexOf(c) == str.lastIndexOf(c)) {
                    return c;
                }
            }
        return 0;

    }

    public static int findMissing (int [] arr){
        if (arr.length < 2){
            return 0;
        }
        Arrays.sort(arr);
        for (int i = 0; i < arr.length; i++) {
            if (arr[i+1] - arr[i] != 1){
                return arr[i]+1;
            }
        }
        return 0;
    }
}