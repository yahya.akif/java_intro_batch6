package projects;

public class Project01 {
    public static void main(String[] args) {

        System.out.println("===============Task-1===========");
        String name = "yhehia Akif";

        System.out.println("My name is = " + name);

        System.out.println("===============Task-2===========");

        char nameCharacter1 = 'y';
        char nameCharacter2 = 'h';
        char nameCharacter3 = 'e';
        char nameCharacter4 = 'h';
        char nameCharacter5 = 'i';
        char nameCharacter6 = 'a';

        System.out.println("Name letter 1 = " + nameCharacter1);
        System.out.println("Name letter 2 = " + nameCharacter2);
        System.out.println("Name letter 3 = " + nameCharacter3);
        System.out.println("Name letter 4 = " + nameCharacter4);
        System.out.println("Name letter 5 = " + nameCharacter5);
        System.out.println("Name letter 6 = " + nameCharacter6);

        System.out.println("=============Task-3============");

        String myFavMovie = "Don't mess with the zohan";
        String myFavSong = "Don't you worry child";
        String myFavCity = "Bodrum";
        String myFavActivity = "Playing football";
        String myFavSnack = "sun chips";

        System.out.println("My favorite movie is = " + myFavMovie);
        System.out.println("My favorite song is = " + myFavSong);
        System.out.println("My favorite city is = " + myFavCity);
        System.out.println("My favorite activity is = " + myFavActivity);
        System.out.println("My favorite snack is = " + myFavSnack);

        System.out.println("==============Task-4==========");

        int myFavNumber = 21;
        int numberOfStatesIVisited = 47;
        int numberOfCountriesIVisited = 9;

        System.out.println("My favorite number is = " + myFavNumber);
        System.out.println("The number of states I visited is = " + numberOfStatesIVisited);
        System.out.println("The number of countries I visited is = " + numberOfCountriesIVisited);

        System.out.println("=============Task-5===========");

        boolean onCampus = true;
        boolean online = false;

        System.out.println("Am I at school today = " + online);

        System.out.println("=============Task-6===========");

        boolean Above60 = true;
        boolean lessOrEqual60 = false;

        System.out.println("Is the weather nice today = " + lessOrEqual60);


        System.out.println("============EndOfProject==========");




    }
}
