package projects;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class Project07 {
    public static void main(String[] args) {
        System.out.println("\n----------------Task-1-----------------\n");

        String[] str = {"foo", "", "", "foo bar", "java is fun", "ruby"};
        System.out.println(countMultipleWords(str));

        System.out.println("\n----------------Task-2-----------------\n");

        ArrayList<Integer> list = new ArrayList<>(Arrays.asList(2, -5, 6, 7, -10, -78, 0, 15));
        System.out.println(removeNegatives(list));

        System.out.println("\n----------------Task-3-----------------\n");

        Scanner s = new Scanner(System.in);
        System.out.println("Please enter your Password");
        String password = s.nextLine();
        System.out.println(validatePassword(password));

        System.out.println("\n----------------Task-4-----------------\n");

        System.out.println("Please enter your Email Adrress");
        String Email = s.nextLine();
        System.out.println(validateEmail(Email));
    }

    public static int countMultipleWords(String[] str) {
        int counter = 0;


        for (String s : str) {
            if (s.trim().split(" ").length > 1) {
                counter++;
                // System.out.println(counter);
            }

        }

        return counter;
    }

    public static ArrayList<Integer> removeNegatives(ArrayList<Integer> list2) {
        ArrayList<Integer> list3 = new ArrayList<>();
        for (int element : list2) {
            if (element >= 0) list3.add(element);
            // if (list.get(i) < 0) list.remove(i--); // this is how to use remove
        }
        return list3;
    }

    public static boolean validatePassword(String str) {
        int digitCount = 0;
        int upperCount = 0;
        int lowerCount = 0;
        int special = 0;

        if (str.length() >= 8 && str.length() <= 16 && !str.contains(" ")) {
            for (int i = 0; i < str.length(); i++) {
                if (Character.isUpperCase(str.charAt(i))) upperCount++;
                else if (Character.isLowerCase(str.charAt(i))) lowerCount++;
                else if (Character.isDigit(str.charAt(i))) digitCount++;
                else special++;
            }
        } else {
            return false;
        }
        if (upperCount > 0 && digitCount > 0 && lowerCount > 0 && special > 0)
            return true;
        else return false;
    }

    public static boolean validateEmail(String str) {

        int atIndex = str.indexOf('@'); // >2
        int dotIndex = str.indexOf('.'); // >5  8

        if (str.contains(" ") || !str.contains("@") || str.contains(".")) {
            return false;
        }
            // ab@yah.com
        if (atIndex >= 2 && dotIndex < atIndex + 3 && str.length() - dotIndex >= 2) {
            return true;
        }

        boolean singleAmp = atIndex == str.lastIndexOf("@");
            return true;


        }



}