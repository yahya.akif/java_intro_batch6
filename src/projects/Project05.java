package projects;

import utitlities.ScannerHelper;

import java.util.Random;

public class Project05 {
    public static void main(String[] args) {

        System.out.println("\n-----------Task-1---------------\n");

        String str = ScannerHelper.getString();
        str = str.trim();
        int countWords = 1;

        for (int i = 0; i < str.length(); i++) {
            if (Character.isWhitespace(str.charAt(i))) {
                countWords++;

            }
        }
        if (countWords >= 1) {
            System.out.println("The sentence has " + countWords + " words.");
        } else System.out.println("This sentence does not have multiple words");


        System.out.println("\n-----------Task-2---------------\n");

        Random r = new Random();
        int num1 = r.nextInt(26);  // limit 0 included - 25 included
        int num2 = r.nextInt(26);

        System.out.println(num1);
        System.out.println(num2);

        for (int x = Math.min(num1, num2); x <= Math.max(num1, num2); x++) {
            if (x % 5 != 0) {
                System.out.print(x + " - ");
            }
        }
        System.out.println("\n-----------Task-3---------------\n");

        String sentence = ScannerHelper.getString();

        int aCounter = 0;

        for (int j = 0; j < sentence.length(); j++) {
            if (sentence.toLowerCase().charAt(j) == 'a') {
                aCounter++;
            }
        }
        if (aCounter < 1) {
            System.out.println("This sentence does not have any characters");
        } else System.out.println("This sentence has " + aCounter + " a or A letters");

        System.out.println("\n-----------Task-4---------------\n");

        String word = ScannerHelper.getString();

        String reverse = "";

        for (int i = word.length() - 1; i >= 0; i--) {
            reverse += word.charAt(i);
        }
        boolean palindrome = true;
        for (int i = 0; i < word.length(); i++) {
            if (word.charAt(i) != reverse.charAt(i)) {
                palindrome = false;
            }
        }
        if (word.length() < 1) {
            System.out.println("This word does not have 1 or more characters");
        } else {
            if (palindrome) {
                System.out.println("This word is palindrome");
            } else {
                System.out.println("This word is not palindrome");
            }


            System.out.println("\n-----------Task-5---------------\n");

            for (int i = 1; i <= 9; i++) {
                for (int j = 1; j <=9 - i; j++) {
                    System.out.print("   ");
                }

                for (int x = 1; x <= i*2-1;x++) {
                    System.out.print(" * ");
                }
                System.out.println();
            }

            }


        }
    }
