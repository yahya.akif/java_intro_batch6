package homeworks;

import java.util.Arrays;

public class Homework06 {
    public static void main(String[] args) {
        System.out.println("\n------------------Task-1-----------------------\n");

        int[] numbers = new int[10];

        System.out.println(Arrays.toString(numbers));

        numbers[2] = 23;
        numbers[4] = 12;
        numbers[7] = 34;
        numbers[9] = 7;
        numbers[6] = 15;
        numbers[0] = 89;

        System.out.println(numbers[3]);
        System.out.println(numbers[0]);
        System.out.println(numbers[9]);
        System.out.println(Arrays.toString(numbers));

        System.out.println("\n------------------Task-2-----------------------\n");

        String[] str = new String[5];

        str[1] = "abc";
        str[4] = "xyz";

        System.out.println(str[3]);
        System.out.println(str[0]);
        System.out.println(str[4]);
        System.out.println(Arrays.toString(str));

        System.out.println("\n------------------Task-3-----------------------\n");

        int[] num = {23, -34, -56, 0, 89, 100};

        System.out.println(Arrays.toString(num));

        Arrays.sort(num);
        System.out.println(Arrays.toString(num));

        System.out.println("\n------------------Task-4-----------------------\n");

        String[] str4 = {"Germany", "Argentina", "Ukraine", "Romania"};

        System.out.println(Arrays.toString(str4));

        Arrays.sort(str4);
        System.out.println(Arrays.toString(str4));

        System.out.println("\n------------------Task-5-----------------------\n");

        String[] str5 = {"Scooby Doo", "Snoopy", "Blue", "Pluto", "Dino", "Sparky"};

        System.out.println(Arrays.toString(str5));

        System.out.println(Arrays.toString(str5).contains("Pluto"));

        System.out.println("\n------------------Task-6-----------------------\n");

        String[] str6 = {"Garfield", "Tom", "Sylvester", "Azrael"};

        Arrays.sort(str6);
        System.out.println(Arrays.toString(str6));

        System.out.println(Arrays.toString(str6).contains("Garfield") && Arrays.toString(str6).contains("Felix"));

        System.out.println("\n------------------Task-7-----------------------\n");

        double[] str7 = {10.5, 20.75, 70, 80, 15.75};

        System.out.println(Arrays.toString(str7));
        System.out.println(str7[0]);
        System.out.println(str7[1]);
        System.out.println(str7[2]);
        System.out.println(str7[3]);
        System.out.println(str7[4]);

        System.out.println("\n------------------Task-8-----------------------\n");

        char[] character = {'A', 'b', 'G', 'H', '7', '5', '&', '*', 'e', '@', '4'};

        int letters = 0;
        int lowerC = 0;
        int upperC = 0;
        int digit = 0;
        int special = 0;


        for (char i = 0; i < character.length; i++) {
            if (Character.isLetter(character[i])) {
                letters++;
                if (Character.isUpperCase(character[i]))
                    upperC++;
                else lowerC++;
            } else if (Character.isDigit(character[i]))
                digit++;
            else special++;
        }

        System.out.println(Arrays.toString(character));
        System.out.println("Letters = " + letters);
        System.out.println("Uppercase Letters = " + upperC);
        System.out.println("Lowercase Letters = " + lowerC);
        System.out.println("Digits = " + digit);
        System.out.println("Special characters = " + special);

        System.out.println("\n------------------Task-9-----------------------\n");

        String[] str9 = {"Pen", "notebook", "Book", "paper", "bag", "pencil", "Ruler"};

        int isUpperCase = 0;
        int isLowerCase = 0;
        int startBorP = 0;
        int haveBookOrPen = 0;

        for (int i = 0; i < str9.length; i++) {
            if (Character.isUpperCase(str9[i].charAt(0))) isUpperCase++;
            else isLowerCase++;
        }
        for (int s = 0; s < str9.length; s++) {
            if (str9[s].toUpperCase().startsWith("B") || str9[s].toUpperCase().startsWith("P")) startBorP++;
        }
        for (int j = 0; j < str9.length; j++) {
                    if (str9[j].toLowerCase().contains("pen") || str9[j].toLowerCase().contains("book")) haveBookOrPen++;
        }



        System.out.println(Arrays.toString(str9));
        System.out.println("Elements start with upper case = " + isUpperCase);
        System.out.println("Elements start with upper case = " + isLowerCase);
        System.out.println("Elements starting with B or P = " + startBorP);
        System.out.println("Elements having book or pen = " + haveBookOrPen);
        System.out.println("\n------------------Task-10-----------------------\n");

        int[] noms = {3, 5, 7, 10, 0, 20, 17, 10, 23, 56, 78};

        System.out.println(Arrays.toString(numbers));

        int moreThan10 = 0;
        int lessThan10 = 0;
        int equal10 = 0;

        for (int i = 0; i < noms.length; i++) {
            if (noms[i] > 10) {
                moreThan10++;
            } else if (noms[i] == 10) {
                equal10++;
            } else lessThan10++;
        }

        System.out.println("Elements that are more than 10 = " + moreThan10);
        System.out.println("Elements that are less than 10 = " + lessThan10);
        System.out.println("Elements that are equal 10 = " + equal10);


        System.out.println("\n------------------Task-11-----------------------\n");

        int[] array1 = {5, 8, 13, 1, 2};
        int[] array2 = {9, 3, 67, 1, 0};
        int[] array3 = new int[5];

        System.out.println("1st array is = " + Arrays.toString(array1));
        System.out.println("2nd array is = " + Arrays.toString(array2));

        for (int i = 0; i < array1.length; i++) {
            array3[i] = Math.max(array1[i], array2[i]);
        }
        System.out.println("3rd array is = " + Arrays.toString(array3));
    }
}
