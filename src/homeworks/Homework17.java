package homeworks;

public class Homework17 {
    public static void main(String[] args) {
        System.out.println("----------Task-1----------");

        System.out.println(nthWord("I like programming language",2));
        System.out.println(nthWord("QA stands for Quality Assurance",4));
        System.out.println(nthWord("Hello World",3));

        System.out.println("----------Task-2----------");

        System.out.println(isArmStrong(153));
        System.out.println(isArmStrong(123));
        System.out.println(isArmStrong(121));

        System.out.println("----------Task-3----------");

        System.out.println(reverseNumber(371));
        System.out.println(reverseNumber(12));
    }

    public static String nthWord(String str,int n){
        String [] word = str.split(" ");

        if (n>word.length-1) return "";
        else return word[n-1];
    }

    public static boolean isArmStrong(int number){
        int originalNumber = number;
        int numOfNum = String.valueOf(number).length();
        int sum = 0;

        while (number > 0) {
            int digit = number % 10;
            sum += Math.pow(digit, numOfNum);
            number /= 10;
        }

        return sum == originalNumber;
    }

    public static int reverseNumber(int number){
        int reversedNumber = 0;

        while (number != 0) {
            int digit = number % 10;
            reversedNumber = reversedNumber * 10 + digit;
            number /= 10;
        }

        return reversedNumber;
    }
}
