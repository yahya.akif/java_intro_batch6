package homeworks;

import java.util.Arrays;

public class Homework18 {
    public static void main(String[] args) {
        System.out.println("---------Task-1--------");

        System.out.println(Arrays.toString(doubleOrTriple(new int[]{1, 5, 10}, true)));
        System.out.println(Arrays.toString(doubleOrTriple(new int[]{3, 7, 2}, false)));

        System.out.println("---------Task-2--------");

        System.out.println(splitString("java", 2));
        System.out.println(splitString("javaScript", 5));
        System.out.println(splitString("Hello", 3));

        System.out.println("---------Task-3--------");
        System.out.println(countPalindrome("mom and dad"));
        System.out.println(countPalindrome("Kayak races attracts racecar drivers"));
    }

    public static int[] doubleOrTriple(int[] arr, boolean x) {
        for (int i = 0; i < arr.length; i++) {
            if (x) {
                arr[i] = arr[i] * 2;
            } else {
                arr[i] = arr[i] * 3;
            }
        }
        return arr;
    }

    public static String splitString(String str, int n) {
        if (str.length() % n == 0) {
           return str.substring(0,n) + " " + str.substring(n);
    } else return "";
    }

    public static int countPalindrome (String str) {
            String[] words = str.split(" ");
            int count = 0;

            for (String word : words) {
                String lowercaseWord = word.toLowerCase();
                if (isPalindrome(lowercaseWord)) {
                    count++;
                }
            }

            return count;

        }

    public static Boolean isPalindrome (String word) {


        String revWord = "";
        for (int i = word.length()-1; i >= 0; i--) {

            revWord += word.charAt(i);
        }
        return  (word.equals(revWord));
    }
    }