package homeworks;

import java.util.ArrayList;
import java.util.Arrays;


public class Homework09 {
    public static void main(String[] args) {
        System.out.println("========Task-1========");

        int[] arr = {-4, 0, -7, 0, 5, 10, 45, 45};
        System.out.println(firstDuplicate(arr));

        System.out.println(firstDuplicate(new int[]{-8, 56, 7, 8, 6}));

        System.out.println("========Task-2========");

        String[] arr2 = {"Z", "abc", "z", "123", "#"};
        System.out.println(stringDuplicate(arr2));

        System.out.println(stringDuplicate(new String[]{"xyz", "java", "abc"}));

        System.out.println("========Task-3========");
        int[] arr3 = {0, -4, -7, 0, 5, 10, 45, -7, 0};
        System.out.println(duplicatedNumbers(arr3));


        System.out.println(duplicatedNumbers(new int[]{1, 2, 5, 0, 7}));

        System.out.println("========Task-4========");

        System.out.println(duplicatedStrings(new String[]{"A", "foo", "12", "Foo", "bar", "a", "java"}));
        System.out.println(duplicatedStrings(new String[]{"python", "foo", "bar", "java", "123"}));

        System.out.println("========Task-5========");

        reverseArray(new String[]{"abc", "foo", "bar"});
        reverseArray(new String[]{"java", "python", "ruby"});

        System.out.println("========Task-6========");

        String str = "Java is fun";
        reverseStringWord(str);
    }

    public static int firstDuplicate(int[] numbers) {
        int duplicate = Integer.MIN_VALUE;
        boolean duplicateBoolean = false;
        for (int i = 0; i < numbers.length - 1; i++) {
            int j;
            for (j = i + 1; j < numbers.length - 1; j++) {
                if (numbers[i] == numbers[j]) {
                    duplicate = numbers[i];
                    duplicateBoolean = true;
                    break;
                }
            }
        }
        if (duplicateBoolean) return duplicate;
        else return -1;

    }

    public static String stringDuplicate(String[] str) {
        String duplicate = "";
        boolean isDuplicate = false;
        for (int i = 0; i < str.length - 1; i++) {
            for (int j = i + 1; j < str.length; j++) {
                if (str[i].equalsIgnoreCase(str[j])) {
                    duplicate = str[i];
                    isDuplicate = true;
                    break;
                }
            }
        }
        if (!isDuplicate) return "There is no duplicates";
        else return duplicate;

    }

    public static ArrayList<Integer> duplicatedNumbers(int[] arr) {
        ArrayList<Integer> dup = new ArrayList<>();
        for (int i = 0; i < arr.length - 1; i++) {
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[i] == arr[j] && !dup.contains(i)) {
                    dup.add(arr[i]);
                }
            }
        }
        return dup;
    }

    public static ArrayList<String> duplicatedStrings(String[] arr) {
        ArrayList<String> duplicate = new ArrayList<>();

        for (int i = 0; i < arr.length; i++) {
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[i].equalsIgnoreCase(arr[j]) && !duplicate.contains(arr[i])) duplicate.add(arr[i]);
            }
        }
        return duplicate;
    }

    public static void reverseArray(String[] arr) {
        String[] reverse = new String[arr.length];

        for (int i = arr.length - 1; i >= 0; i--) {
            reverse[i] = arr[i];
        }
        System.out.println(Arrays.toString(reverse));
    }

    public static void reverseStringWord(String str) {
        String[] words = str.split("\\s+");
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < words.length; i++) {
            StringBuilder reverseWord = new StringBuilder(words[i]).reverse();
            result.append(reverseWord);
            if (i < words.length - 1) {
                result.append(" ");

            }
        }
        System.out.println(result);
    }
}
