package homeworks;

public class HomeWork14 {
    public static void main(String[] args) {

        System.out.println("----------Task-1---------");

        fizzBuzz1(3);
        fizzBuzz1(5);
        fizzBuzz1(18);

        System.out.println("----------Task-2---------");

        System.out.println(fizzBuzz2(0));
        System.out.println(fizzBuzz2(1));
        System.out.println(fizzBuzz2(3));
        System.out.println(fizzBuzz2(5));

        System.out.println("----------Task-3---------");

        System.out.println(findSumNumbers("abc$"));
        System.out.println(findSumNumbers("a1b4c  6#"));
        System.out.println(findSumNumbers("ab110c045d"));

        System.out.println("----------Task-4---------");

        System.out.println(findBiggestNumber("abc$"));
        System.out.println(findBiggestNumber("a1b4c  6#"));
        System.out.println(findBiggestNumber("ab110c045d"));

        System.out.println("----------Task-5---------");

        System.out.println(countSequenceOfCharacters(""));
        System.out.println(countSequenceOfCharacters("abbcca"));
        System.out.println(countSequenceOfCharacters("aaAAa"));

    }

    public static void fizzBuzz1(int a){
        for (int i = 1; i <= a; i++) {
            if (i%15 == 0) {
                System.out.println("FizzBuzz");
            } else if (i%3 == 0) {
                System.out.println("Fizz");
            }
            else if (i%5 == 0){
                System.out.println("Buzz");
            }

            else System.out.println(i);
        }
    }

    public static String fizzBuzz2(int a) {
            if (a % 15 == 0) {
                return ("FizzBuzz");
            } else if (a % 3 == 0) {
                return ("Fizz");
            } else if (a % 5 == 0) {
                return ("Buzz");
            } else return "" + a;
    }

        public static int findSumNumbers(String str){
            if (str.isEmpty()) {
                return 0;
            }

            String[] numbers = str.replaceAll("[^0-9]", " ").split(" ");
            int sum = 0;

            for (String num : numbers) {
                if (!num.isEmpty()) {
                    sum += Integer.parseInt(num);
                }
            }

            return sum;
        }

        public static int findBiggestNumber(String str) {

            if (str.isEmpty()) {
                return 0;
            }

            String[] numbers = str.replaceAll("[^0-9]", " ").split(" ");
            int largestNumber = 0;

            for (String num : numbers) {
                if (!num.isEmpty() && (Integer.parseInt(num) > largestNumber)) {
                    largestNumber = Integer.parseInt(num);
                }
            }
                return largestNumber;

            }

        public static String countSequenceOfCharacters(String str){
        String countLetter = "";
        int count = 0;

            for (int i = 0; i < str.length(); i++) {
                count++;
                if(i == str.length()-1 || str.charAt(i) != str.charAt(i+1)){
                    countLetter+= count+ String.valueOf(str.charAt(i));
                    count =0;
                }
            }
        return countLetter;
        }
}

