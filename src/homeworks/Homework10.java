package homeworks;

import java.util.ArrayList;
import java.util.Arrays;

public class Homework10 {
    public static void main(String[] args) {
        System.out.println("=======Task-1========");

        System.out.println(countWords(new String("Selenium is the most common UI automation tool.   ")));
        System.out.println("=======Task-2========");

        System.out.println(countA(new String("echGlobal is a QA bootcamp")));
        System.out.println(countA(new String("QA stands for Quality Assurance")));
        System.out.println("=======Task-3========");

        System.out.println(countPos(new ArrayList<>(Arrays.asList(-45, 0, 0, 34, 5, 67))));
        System.out.println(countPos(new ArrayList<>(Arrays.asList(-23, -4, 0, 2, 5, 90, 123))));

        System.out.println("=======Task-4========");

        System.out.println(removeDuplicate(new ArrayList<>(Arrays.asList(10, 20, 35, 20, 35, 60, 70, 60))));
        System.out.println(removeDuplicate(new ArrayList<>(Arrays.asList(1, 2, 5, 2, 3))));

        System.out.println("=======Task-5========");

        System.out.println(removeDuplicateElements(new ArrayList<>(Arrays.asList("java", "C#", "ruby", "JAVA", "ruby", "C#", "C++"))));
        System.out.println(removeDuplicateElements(new ArrayList<>(Arrays.asList("abc", "xyz", "123", "ab", "abc", "ABC"))));

        System.out.println("=======Task-6========");

        System.out.println(removeExtraSpaces("  I   am      learning     Java      "));
        System.out.println(removeExtraSpaces("java  is fun    "));

        System.out.println("=======Task-7========");

        System.out.println(Arrays.toString(add(new int[]{3, 0, 0, 7, 5, 10},new int[]{6,3,2})));
        System.out.println(Arrays.toString(add(new int[]{10, 3, 6, 3, 2},new int[]{6, 8, 3, 0, 0, 7, 5, 10, 34})));

        System.out.println("=======Task-8========");

        System.out.println(findClosestTo10(new int[] {10, -13, 5, 70, 15, 57}));
        System.out.println(findClosestTo10(new int[] {10, -13, 8, 12, 15, -20}));
    }

    public static int countWords(String str) {
        return str.trim().split(" ").length;
    }

    public static int countA(String str){
        int count =0;
        for (int i = 0; i <str.length() ; i++) {
            if (str.charAt(i) == 'A' || str.charAt(i) =='a') count++;
        }
        return count;
    }

    public static int countPos (ArrayList<Integer> list){
        int count =0;
        ArrayList<Integer> countList = new ArrayList<>();
        for (int e:list) {
            if (e>0) count++;
        }
        return count;
    }

    public static ArrayList<Integer> removeDuplicate(ArrayList<Integer> list){
        ArrayList<Integer> duplicate = new ArrayList<>();
        for (int e:list) {
            if (!duplicate.contains(e)) duplicate.add(e);
        }
        return duplicate;
    }

    public static ArrayList<String> removeDuplicateElements(ArrayList<String> str){
        ArrayList<String> dup = new ArrayList<>();
        for (String s:str) {
            if (!dup.contains(s)) dup.add(s);
        }
        return dup;
    }

    public static String removeExtraSpaces(String str){
       return str.replaceAll("\\s+"," ").trim();
    }

    public static int [] add(int [] arr, int [] arr2){
        int shortest = Math.min(arr.length,arr2.length);
        int longest = Math.max(arr.length,arr2.length);

        int [] biggerArray;
        if (arr.length>arr2.length) {
            biggerArray = arr;
        }
        else biggerArray = arr2;

        int[] sum = new int[longest];
        for (int i = 0; i < longest; i++) {
            if (i < shortest) {
                sum[i] = arr[i] + arr2 [i];
            }
            else sum[i] = biggerArray[i];
        }
        return sum;
    }

    public static int findClosestTo10 (int [] arr){
        Arrays.sort(arr);
        int closest = Integer.MAX_VALUE;
        for (int i:arr) {
            if (i == 10){
                continue;
            } else if (Math.abs(i-10) < Math.abs(closest-10)) {
                closest = i;
            }
        }
        return closest;
    }
}
