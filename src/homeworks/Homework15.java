package homeworks;

import java.util.*;

public class Homework15 {
    public static void main(String[] args) {
        System.out.println("---------Task-1---------");

        System.out.println(Arrays.toString(fibonacciSeries1(3)));
        System.out.println(Arrays.toString(fibonacciSeries1(5)));
        System.out.println(Arrays.toString(fibonacciSeries1(7)));

        System.out.println("---------Task-2---------");

        System.out.println(fibonacciSeries2(2));
        System.out.println(fibonacciSeries2(4));
        System.out.println(fibonacciSeries2(8));

        System.out.println("---------Task-3---------");

        System.out.println(Arrays.toString(findUniques(new int[]{}, new int[]{})));
        System.out.println(Arrays.toString(findUniques(new int[]{}, new int[]{1,2,3,2})));
        System.out.println(Arrays.toString(findUniques(new int[]{1,2,3,4}, new int[]{3,4,5,5})));
        System.out.println(Arrays.toString(findUniques(new int[]{8,9}, new int[]{9,8,9})));

        System.out.println("---------Task-4---------");

        System.out.println(firstDuplicate(new int[]{}));
        System.out.println(firstDuplicate(new int[]{1}));
        System.out.println(firstDuplicate(new int[]{1, 2, 2, 3}));
        System.out.println(firstDuplicate(new int[]{1, 2, 3, 3, 4, 1}));

        System.out.println("---------Task-5---------");

        System.out.println(isPowerOf3(1));
        System.out.println(isPowerOf3(2));
        System.out.println(isPowerOf3(3));
        System.out.println(isPowerOf3(81));
    }

    public static int[] fibonacciSeries1(int n) {
        int[] arr = new int[n];

        arr[0] = 0;
        arr[1] = 1;

        for (int i = 2; i < n; i++) {
            arr[i] = arr[i - 2] + arr[i - 1];
        }
        return arr;
    }

    public static int fibonacciSeries2(int n) {
        int[] arr = new int[n];

        arr[0] = 0;
        arr[1] = 1;

        for (int i = 2; i < n; i++) {
            arr[i] = arr[i - 2] + arr[i - 1];
        }
        return arr[n - 1];
    }

    public static int[] findUniques(int[] array1, int[] array2) {
            Set<Integer> set1 = new HashSet<>();
            Set<Integer> set2 = new HashSet<>();


            for (int num : array1) {
                set1.add(num);
            }

            for (int num : array2) {
                set2.add(num);
            }
        HashSet<Integer> set1Container = new HashSet<>(set1);

            set1.removeAll(set2);
            set2.removeAll(set1Container);
            set1.addAll(set2);

            int i =0;
            int[] answer = new int[set1.size()];
        for (int n:set1) {
            answer[i] = n;
            i++;
        }

            return answer;

    }

    public static int firstDuplicate(int[] arr) {
        int duplicate = -1;
        Arrays.sort(arr);
        for (int i = 0; i < arr.length - 1; i++) {
            if (arr[i] == arr[i + 1]) {
                duplicate = arr[i];
            }
        }
        return duplicate;
    }

    public static boolean isPowerOf3(int n) {
        if (n == 1) {
            return true;
        } else return n % 3 == 0;
    }
}



