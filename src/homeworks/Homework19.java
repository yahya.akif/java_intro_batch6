package homeworks;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class Homework19 {
    public static void main(String[] args) {
        System.out.println("----------Task-1--------");

        System.out.println(sum(new int[]{1, 5, 10}, true));
        System.out.println(sum(new int[]{3, 7, 2, 5, 10}, false));

        System.out.println("----------Task-2--------");

        System.out.println(nthChars("Java",2));
        System.out.println(nthChars("Java",3));
        System.out.println(nthChars("JavaScript",5));
        System.out.println(nthChars("Hi",4));

        System.out.println("----------Task-3--------");

        System.out.println(canFormString("Hello","Hi"));
        System.out.println(canFormString("programming", "gaming"));
        System.out.println(canFormString("halogen", "hello"));
        System.out.println(canFormString("CONVERSATION", "voices rant on"));

        System.out.println("----------Task-4--------");
        System.out.println(isAnagram("Apple", "Peach")); // Expected Output: false

        // Test Data 2
        System.out.println(isAnagram("listen", "silent"));

        // Test Data 3
        System.out.println(isAnagram("astronomer", "moon starer"));

        // Test Data 4
        System.out.println(isAnagram("CINEMA", "iceman")); // Expected Output: true
    }

    public static int sum(int[] arr, boolean x) {
        int count = 0;
        for (int i = 0; i < arr.length; i++) {
            if ((x && i % 2 == 0) || (!x && i % 2 == 1))
                count += arr[i];
        }
        return count;
    }
        public static String nthChars(String str, int n){

           String result = "";

            for (int i = n - 1; i < str.length(); i += n) {
              result += str.charAt(i);
            }

            return result;
        }

        public static boolean canFormString(String str1, String str2){
            char[] arr1 = str1.toLowerCase().toCharArray();
            char[] arr2 = str2.toLowerCase().toCharArray();

            for (int i = 0; i < arr2.length; i++) {
                boolean isValid = false;
                for (int j = 0; j < arr1.length; j++) {
                    if(arr1[j] == arr2[i]){
                        isValid = true;
                        arr1[j] = ' ';
                        break;
                    }
                }
                if (!isValid) return false;
            }
            return true;
        }

    public static boolean isAnagram(String str1, String str2) {
        char[] arr1 = str1.toLowerCase().toCharArray();
        char[] arr2 = str2.toLowerCase().toCharArray();

        Arrays.sort(arr1);
        Arrays.sort(arr2);
        return Arrays.equals(arr1,arr2);
    }

}