package homeworks;

import java.util.ArrayList;
import java.util.Arrays;

public class Homework13 {
    public static void main(String[] args) {
        System.out.println("------Task-1--------");

        System.out.println(hasLowerCase(""));
        System.out.println(hasLowerCase("JAVA"));
        System.out.println(hasLowerCase("hello"));

        System.out.println("------Task-2--------");

        System.out.println(noZero(new ArrayList<Integer>(Arrays.asList(1,0,1))));
        System.out.println(noZero(new ArrayList<Integer>(Arrays.asList(10,0,0,1))));
        System.out.println(noZero(new ArrayList<Integer>(Arrays.asList(5,0,1))));

        System.out.println("------Task-3--------");

        System.out.println(Arrays.deepToString(numberAndSquare(new int[]{1,2,3})));
        System.out.println(Arrays.deepToString(numberAndSquare(new int[]{0,3,6})));
        System.out.println(Arrays.deepToString((numberAndSquare(new int[]{1,4}))));

        System.out.println("------Task-4--------");

        System.out.println(containsValue(new String[]{"abc", "foo", "java"}, "hello"));
        System.out.println(containsValue(new String[]{"abc", "def", "123"}, "ABC"));
        System.out.println(containsValue(new String[]{"abc", "def", "123"}, "abc"));

        System.out.println("------Task-5--------");

        System.out.println(reverseSentence("Java is fun"));
        System.out.println(reverseSentence("Hello"));
        System.out.println(reverseSentence("This is a sentence"));

        System.out.println("------Task-6--------");

        System.out.println(removeStringSpecialsDigits("123Java #$%is fun"));
        System.out.println(removeStringSpecialsDigits("Selenium"));
        System.out.println(removeStringSpecialsDigits("Selenium 123#$%Cypres"));

        System.out.println("------Task-7--------");

        System.out.println(Arrays.toString(removeArraySpecialsDigits(new String[]{"123Java", "#$%is", "fun"})));
        System.out.println(Arrays.toString(removeArraySpecialsDigits(new String[]{"Selenium", "123$%", "###"})));
        System.out.println(Arrays.toString(removeArraySpecialsDigits(new String[]{"Selenium", "123#$%Cypress"})));

        System.out.println("------Task-8--------");

        System.out.println(removeAndReturnCommons(new ArrayList<>(Arrays.asList("Java", "is", "fun")),
                new ArrayList<>(Arrays.asList("abc", "xyz", "123"))));
        System.out.println(removeAndReturnCommons(new ArrayList<>(Arrays.asList("Java", "is", "fun")),
                new ArrayList<>(Arrays.asList("Java", "C#", "Python"))));
        System.out.println(removeAndReturnCommons(new ArrayList<>(Arrays.asList("Java", "C#", "C#")),
                new ArrayList<>(Arrays.asList("Python", "C#", "C++"))));

        System.out.println("------Task-9--------");

        System.out.println(noXInVariables(new ArrayList<>(Arrays.asList("abc", "123", "#$%"))));
        System.out.println(noXInVariables(new ArrayList<>(Arrays.asList("x", "123", "#$%"))));
        System.out.println(noXInVariables(new ArrayList<>(Arrays.asList("xyXyxy", "Xx", "ABC"))));
    }

    public static boolean hasLowerCase(String str){
        for (int i = 0; i < str.length(); i++) {
            return (Character.isLowerCase(str.charAt(i)));
        }
        return false;
    }

    public static ArrayList<Integer> noZero(ArrayList<Integer> list){

        list.removeIf(i -> i == 0);
        return list;
    }

    public static int[][] numberAndSquare (int [] arr){
        int [][] arr2 = new int[arr.length][2];
        for (int i = 0; i < arr.length; i++) {
           arr2[i][0] = arr[i];
           arr2[i][1] = arr[i]*arr[i];
        }
        return arr2;
    }

    public static boolean containsValue(String[] arr, String str){
        for (String s : arr) {
            if (s.equals(str)) return true;
        }
        return false;
    }

    public static String reverseSentence(String str){
        String [] str1 = str.split(" ");

        String [] str2 = new String[str1.length];
        if (str1.length >1){
            int index = 0;

            for (int i = str1.length - 1; i >= 0; i--) {
                str2[index] = str1[i].toLowerCase();
                index++;
            }
            str2[0] = Character.toUpperCase(str2[0].charAt(0)) +str2[0].substring(1);
            return String.join(" ", str2);
        }
        else {
            return "There is not enough words";
        }
    }

    public static String removeStringSpecialsDigits (String str){
        return str = str.replaceAll("[^A-Za-z ]", "");
    }

    public static String [] removeArraySpecialsDigits(String [] arr){
        int index =0;
        for (String a:arr) {
            a = a.replaceAll("[^A-Za-z ]", "");
            arr[index]= a;
            index++;
        }
        return arr;
    }

    public static ArrayList<String>removeAndReturnCommons(ArrayList<String> list1, ArrayList<String> list2){
        ArrayList<String>list3 = new ArrayList<>();
        for (String l:list1) {
            for (String l2:list2) {
                if(l.equals(l2) && !list3.contains(l)) list3.add(l);
            }
        }
        return list3;
    }

    public static ArrayList<String> noXInVariables(ArrayList<String> list){
        int index = 0;

        for (String i : list) {
            i = i.replaceAll("[Xx]", "");
            list.set(index, i);
            index++;
        }
        list.removeIf(str -> str.isEmpty());

        return list;
    }
}
