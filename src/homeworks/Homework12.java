package homeworks;


import java.util.Arrays;


public class Homework12 {
    public static void main(String[] args) {
        System.out.println("-------Task-1--------");

        System.out.println(noDigit("123Hello"));
        System.out.println(noDigit("123Hello World149"));

        System.out.println("-------Task-2--------");

        System.out.println(noVowel("JAVA"));
        System.out.println(noVowel("TechGlobal"));

        System.out.println("-------Task-3--------");

        System.out.println(sumOfDigits(""));
        System.out.println(sumOfDigits("java"));
        System.out.println(sumOfDigits("John's age is 29"));
        System.out.println(sumOfDigits("125.0"));

        System.out.println("-------Task-4--------");

        System.out.println(hasUpperCase(""));
        System.out.println(hasUpperCase("java"));
        System.out.println(hasUpperCase("John's age is 29"));

        System.out.println("-------Task-5--------");

        System.out.println(middleInt(5,3,5));
        System.out.println(middleInt(5,5,8));
        System.out.println(middleInt(-1,25,10));

        System.out.println("-------Task-6--------");

        System.out.println(Arrays.toString(no13(new int[]{1,2,3, 4})));
        System.out.println(Arrays.toString(no13(new int[]{13, 2, 3})));
        System.out.println(Arrays.toString(no13(new int[]{13, 13, 13, 13})));

        System.out.println("-------Task-7--------");

        System.out.println(Arrays.toString(arrFactorial(new int[]{1, 2, 3, 4})));
        System.out.println(Arrays.toString(arrFactorial(new int[]{0,5})));
        System.out.println(Arrays.toString(arrFactorial(new int[]{5,0,6})));
        System.out.println(Arrays.toString(arrFactorial(new int[]{})));

        System.out.println("-------Task-8--------");

        System.out.println(Arrays.toString(categorizeCharacters("abc123$#%")));
        System.out.println(Arrays.toString(categorizeCharacters("12ab$%3c%")));
        System.out.println(Arrays.toString(categorizeCharacters("")));
    }

    public static String noDigit(String str){
        return str.replaceAll("[0-9]","");
    }

    public static String noVowel(String str){
        return str.replaceAll("[AEIOUaeiou]","");
    }

    public static int sumOfDigits(String str){
        int sum = 0;
        str = str.replaceAll("[^1-9]","");

        for (char c:str.toCharArray()){
                sum+= Integer.parseInt("" + c);
        }
        return sum;
    }

    public static boolean hasUpperCase(String str){
        for (int i = 0; i < str.length(); i++) {
            return Character.isUpperCase(str.charAt(i));
        }
        return false;
    }

    public static int middleInt(int num, int num2, int num3){
        int [] arr = {num,num2,num3};
        Arrays.sort(arr);
        return arr[1];
    }

    public static int [] no13(int [] arr){
        for (int i = 0; i < arr.length; i++) {
            if(arr[i] == 13) arr[i]=0;
        }
        return arr;
    }

    public static int [] arrFactorial(int [] arr){
        for (int i = 0; i < arr.length; i++) {
            int not0 = 1;
            for (int j = 1; j <= arr[i]; j++) {
                not0 *= j;
            }
            arr[i] = not0;
        }
      return arr;
    }

    public static String [] categorizeCharacters(String str){
        String[] arr = {str.replaceAll("[^A-Za-z]",""), str.replaceAll("[^0-9]","")
                ,str.replaceAll("[A-Za-z0-9]","")};
        return arr;
    }
}
