package homeworks;

import utitlities.ScannerHelper;

public class Homework04 {
    public static void main(String[] args) {

        System.out.println("=============Task-1=============");

        String name = ScannerHelper.getFirstName();

        System.out.println("The length of the name is = " + name.length()); // name length
        System.out.println("The first character in the name is = " + name.charAt(0)); // name starts with
        System.out.println("The last character in the name is = " + name.charAt(name.length()-1)); // name ends with
        System.out.println("The first 3 characters in the name are = " + name.substring(0,3)); // First 3 letters of name
        System.out.println("The first 3 characters in the name are = " + name.substring(name.length()-3));
        char startWith = name.charAt(0);
        if (name.startsWith("A") || name.startsWith("a")) {
            System.out.println("You are in the club!");
        } else {
            System.out.println("Sorry, you are not in the club!");

            System.out.println("=============Task-2=============");

            String address = ScannerHelper.getAddress();

            if (address.contains("Chicago")) {
                System.out.println("You are in the club");
            } else if (address.contains("Des Plaines")) {
                System.out.println("You are welcome to join the club");
            } else System.out.println("Sorry, you will never be in the club");


        System.out.println("=============Task-3=============");

        String favCountry = ScannerHelper.favCountry();
            if (favCountry.toLowerCase().contains("a") && favCountry.contains("i")) {
                System.out.println("A and i are There");
            } else if (favCountry.toLowerCase().contains("a")) {
                System.out.println("A is there");
            } else if (favCountry.toLowerCase().contains("i")) {
                System.out.println("I is there");
            } else
                System.out.println("A and i are not there");
            }


         System.out.println("=============Task-4=============");

         String str = "     Java is Fun      ";

            System.out.println("The first word in the str is = " + str.trim().toLowerCase().substring(0,4));
            System.out.println("The second word in the str is = " + str.trim().toLowerCase().substring(5,7));
            System.out.println("The third word in the str is = " + str.trim().toLowerCase().substring(8));

        }
    }

