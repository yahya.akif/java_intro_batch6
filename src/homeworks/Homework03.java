package homeworks;

import java.util.Scanner;

public class Homework03 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("==============Task-1==============");
        int n1, n2;
        System.out.println("Please enter number 1? ");
        n1 = input.nextInt();

        System.out.println("Please enter number 2? ");
        n2 = input.nextInt();

        System.out.println("The difference between numbers is = " + Math.abs(n1-n2));

        System.out.println("==============Task-2==============");

        int n3, n4, n5, n6, n7, n8, n9;

        System.out.println("Please enter number 1? ");
        n3 = input.nextInt();

        System.out.println("Please enter number 2? ");
        n4 = input.nextInt();

        System.out.println("Please enter number 3? ");
        n5 = input.nextInt();

        System.out.println("Please enter number 4? ");
        n6 = input.nextInt();

        System.out.println("Please enter number 5? ");
        n7 = input.nextInt();

        n8 = Math.max(n6,n7);
        n9 = Math.min(n6,n7);

        System.out.println("Max value = " + Math.max(Math.max(n3,n4), Math.max(n5,n8)));

        System.out.println("Min value = " + Math.min(Math.min(n3,n4), Math.min(n5,n9)));

        System.out.println("==============Task-3==============");

        int num1, num2, num3;
        num1 = (int)(Math.random()*50 + 51);
        num2 = (int)(Math.random()*50 + 51);
        num3 = (int)(Math.random()*50 + 51);
        System.out.println("Number 1 = " + num1 + "\nNumber 2 = " + num2 + "\nNumber 3 = " +
                num3 + "\nThe sum of numbers = " + (num1+num3+num2) );

        System.out.println("==============Task-4==============");

        double moneyGiven;
        System.out.println("Please enter how much money alex gave to mike? ");
        moneyGiven = input.nextDouble();

        System.out.println("Alex's money: $" + (125 - moneyGiven) + "\nMike's money: $" + (220 + moneyGiven));

        System.out.println("==============Task-5==============");

        double dailySaving;
        System.out.println("Please enter how much money you can save a day?");
        dailySaving = input.nextDouble();

        System.out.println((int)(390/dailySaving));

        System.out.println("==============Task-6==============");

        String s1 = "5", s2 = "10";
        int d1, d2;
        d1 = Integer.parseInt(s1); // converted to an integer an assigned d1,d2 to make the code cleaner
        d2 = Integer.parseInt(s2);

        System.out.println("Sum of " + d1 + " and " + d2 + " is = " + (d1 + d2));

        System.out.println("Product of " + d1 + " and " + d2 + " is = " + (d1 * d2));

        System.out.println("Division of " + d1 + " and " + d2 + " is = " + (d1 / d2));

        System.out.println("Subtraction of " + d1 + " and " + d2 + " is = " + (d1 - d2));

        System.out.println("Remainder of " + d1 + " and " + d2 + " is = " + (d1 % d2));

        System.out.println("==============Task-7==============");

        String s3 = "200", s4 = "-50";
        int d3, d4;

        d3 = Integer.parseInt(s3);
        d4 = Integer.parseInt(s4);

        System.out.println("The greatest value is = " + Math.max(d3,d4));

        System.out.println("The smallest value is = " + Math.min(d3,d4));

        System.out.println("The average is = " + (d3+d4)/2);

        System.out.println("The absolute difference is = " + Math.abs(d3-d4));

        System.out.println("==============Task-8==============");

        System.out.println((int)(24/.96) + " days");
        System.out.println((int)(168/.96) + " days");
        System.out.println("you will save = $" + 150*.96);

        System.out.println("==============Task-9==============");

        double dailyS; // Per day money one can save
        System.out.println("Please enter how much money you can save in a day?");
        dailyS = input.nextDouble();

        System.out.println((int) (1250/dailyS));

        System.out.println("==============Task-10==============");

        double option1 = 475.50;
        double option2 = 951;

        System.out.println("Option 1 will take : " + (int)(14265/option1) + "months");
        System.out.println("Option 2 will take : " + (int)(14265/option2) + "months");

        System.out.println("==============Task-11==============");

        int g1, g2;
        System.out.println("Please enter the first number?");
        g1 = input.nextInt();
        System.out.println("Please enter the first number?");
        g2 = input.nextInt();

        System.out.println((double)g1/g2);

        System.out.println("==============Task-12==============");

        int random1 = (int) (Math.random() * 101);
        int random2 = (int) (Math.random() * 101);
        int random3 = (int) (Math.random() * 101);

        System.out.println(random1);
        System.out.println(random2);
        System.out.println(random3);

        if (random1 > 25 && random2 > 25 && random3 > 25) {
            System.out.println("True");
        } else {
            System.out.println("False");
        }

        System.out.println("==============Task-13==============");

        System.out.println("Please enter a number between 1-7? ");
        int inputNumber;
        inputNumber = input.nextInt();

        if (inputNumber == 1) {
            System.out.println("MONDAY");
        } else if (inputNumber == 2) {
            System.out.println("TUESDAY");
        } else if (inputNumber == 3) {
            System.out.println("WEDNESDAY");
        } else if (inputNumber == 4) {
            System.out.println("THURSDAY");
        } else if (inputNumber == 5) {
            System.out.println("FRIDAY");
        } else if (inputNumber == 6) {
            System.out.println("SATURDAY");
        } else if (inputNumber == 7) {
            System.out.println("SUNDAY");
        } else {
            System.out.println("The number entered is invalid, try again!!");
    }

        System.out.println("==============Task-14==============");
        int result1, result2, result3;

        System.out.println("Please enter you result for exam 1? ");
        result1 = input.nextInt();

        System.out.println("Please enter you result for exam 2? ");
        result2 = input.nextInt();

        System.out.println("Please enter you result for exam 3? ");
        result3 = input.nextInt();

        if ((result1+result2+result3)/3 >= 70) {
            System.out.println("YOU PASSED!");
        } else {
            System.out.println("YOU FAILED!");
        }

        System.out.println("==============Task-15==============");

        int number1, number2, number3;

        System.out.println("Please enter number 1? ");
        number1 = input.nextInt();

        System.out.println("Please enter number 2? ");
        number2 = input.nextInt();

        System.out.println("Please enter number 3? ");
        number3 = input.nextInt();

        if (number1 == number2 && number1 == number3) {
            System.out.println("TRIPLE MATCH");
        }else if (number1 == number2 || number1 == number3 || number2 == number3) {
            System.out.println("DOUBLE MATCH");
        }else {
            System.out.println("NO MATCH");
        }

        System.out.println("END OF PROGRAM");



}}
