package homeworks;

import utitlities.ScannerHelper;

public class Homework05 {
    public static void main(String[] args) {
        System.out.println("\n================Task-1===============");

        for (int i = 1; i <=100 ; i++) {
            if (i % 7 == 0) {
                System.out.print(i + " - ");
            }
        }
        System.out.println("\n================Task-2===============");

        for (int i = 1; i <= 50 ; i++) {
            if (i % 6 == 0) {
                System.out.print(i + " - ");
            }
        }
        System.out.println("\n================Task-3===============");

        for (int i = 100; i >= 50 ; i--) {
            if (i % 5 == 0) {
                System.out.print(i + " - ");
            }
        }
        System.out.println("\n================Task-4===============");

        for (int i = 0; i <8 ; i++) {
            System.out.println("The square of " + i + " is = " + (i*i));
        }
        System.out.println("\n================Task-5===============");

        int sum = 0;

        for (int i = 1; i <= 10 ; i++) {
            sum += i;
        }

        System.out.println("sum = " + sum);

        System.out.println("\n================Task-6===============");

        int input = ScannerHelper.getNumber();
        int factorial = 1;

        for (int i = 1; i <= input ; i++) {
            factorial *= i;
        }

        System.out.println(factorial);

        System.out.println("\n================Task-7===============");

        String fullName = ScannerHelper.getString();

        int vowelC = 0;
        char c;

        for (int i = 0; i < fullName.length(); i++) {
            c = fullName.toLowerCase().charAt(i);
        if (c == 'a' || c == 'e' || c == 'i' || c == 'o' || c== 'u') {
            vowelC++;
        }
        }

        System.out.println("There are " + vowelC + " vowel letter in this full name");

        System.out.println("\n================Task-8===============");


        String fName = ScannerHelper.getFirstName();
        while (fName.toLowerCase().charAt(0) != 'j') {
            fName = ScannerHelper.getFirstName();
        }
        System.out.println("End Of Program");

//        String xname;
//        do {
//            xname = ScannerHelper.getFirstName();
//        } while (xname.toLowerCase().charAt(0) != 'j');
//        System.out.println("End Of Program");


        System.out.println("\n================End=Of=Program===============");
    }
}
