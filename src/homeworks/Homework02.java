package homeworks;

import java.util.Scanner;

public class Homework02 {
    public static void main(String[] args) {

        System.out.println("============Task-1=============");

        Scanner input = new Scanner(System.in);
        int number1, number2, sum;

        System.out.println("Please enter number 1 = ");
        number1 = input.nextInt();

        System.out.println("please enter number 2 = ");
        number2 = input.nextInt();

        sum = number1 + number2;
        System.out.println("The sum of number 1 and 2 entered by user is = " + sum); // shows the sum of number 1 and 2

        System.out.println("============Task-2=============");

        int n1, n2;

        System.out.println("Please enter number 1 = ");
        n1 = input.nextInt();

        System.out.println("Please enter number 2 = ");
        n2 = input.nextInt();

        System.out.println("The product of the 2 given numbers is: " + (n1 * n2));

        System.out.println("============Task-3=============");

        double x1, x2;

        System.out.println("Please enter number 1 = ");
        x1 = input.nextDouble();
        System.out.println("Please enter number 2 = ");
        x2 = input.nextDouble();

        System.out.println("The sum of the given numbers is = " + (x1 + x2));
        System.out.println("The product of the given numbers is = " + x1 * x2);
        System.out.println("The subtraction of the given numbers is = " + (x1 - x2));
        System.out.println("The division of the given numbers is = " + (x1/x2));
        System.out.println("The remainder of the given numbers is = " + x1%x2);

        System.out.println("============Task-4=============");

        int x = -10 + (7 * 5);
        System.out.println("1.     " +  x);
        int y = (72+24) % 24;
        System.out.println("2.     " + y);
        int z = 10 + (-3*9)/9;
        System.out.println("3.     " + z);
        int w = 5 + 18 / 3 * 3 - (6 % 3);
        System.out.println("4.     " + w);
        System.out.println("============Task-5=============");

        int y1, y2;

        System.out.println("Please enter number 1 = ");
        y1 = input.nextInt();
        System.out.println("Please enter number 2 = ");
        y2 = input.nextInt();

        System.out.println("The average of the given numbers is: " + (y1+y2)/2); // Gets the average of both numbers

        System.out.println("============Task-6=============");

        int z1, z2, z3, z4, z5, total;

        System.out.println("Please enter number 1 = ");
        z1 = input.nextInt();
        System.out.println("Please enter number 2 = ");
        z2 = input.nextInt();
        System.out.println("Please enter number 3 = ");
        z3 = input.nextInt();
        System.out.println("Please enter number 4 = ");
        z4 = input.nextInt();
        System.out.println("Please enter number 5 = ");
        z5 = input.nextInt();

        total = z1 + z2 + z3 + z4 + z5;

        System.out.println("The average of the given number is: " + total/5 );

        System.out.println("============Task-7=============");

        int s1, s2, s3;

        System.out.println("Please enter number 1 = ");
        s1 = input.nextInt();
        System.out.println("Please enter number 2 = ");
        s2 = input.nextInt();
        System.out.println("Please enter number 3 = ");
        s3 = input.nextInt();

        System.out.println("The " + s1 + " multiplied with " + s1 + " = " + s1 * s1);
        System.out.println("The " + s2 + " multiplied with " + s2 + " = " + s2 * s2);
        System.out.println("The " + s3 + " multiplied with " + s3 + " = " + s3 * s3);

        System.out.println("============Task-8=============");

        int q1;

        System.out.println("Please enter the side of a square");
        q1 = input.nextInt();
        input.nextLine();

        System.out.println("perimeter of the square = " + (q1 * 4));
        System.out.println("Area of the square = " + q1 * q1);

        System.out.println("============Task-9=============");

        double salary = 90000;

        System.out.println("A software Engineer in Test can earn $" + (salary * 3) + " in 3 years.");
        System.out.println("============Task-10=============");

        String favBook, favColor;
        int favNumber;


        System.out.println("Please enter your favorite book");
        favBook = input.nextLine();

        System.out.println("Please enter your favorite color");
        favColor = input.next();

        System.out.println("please enter your favorite number");
        favNumber = input.nextInt();

        System.out.println("\t User's favorite book is : " + favBook + "\n\t User's favorite color is : " + favColor + "\n\t User's favorite number is : " + favNumber);

        System.out.println("============Task-11============");

        String firstName, lastName, phoneNumber, emailAddress, address;
        int age;

        System.out.println("Please enter your first name = ");
        firstName = input.next();
        System.out.println("Please enter your last name= ");
        lastName = input.next();
        System.out.println("please enter your Age = ");
        age = input.nextInt();
        input.nextLine();
        System.out.println("Please enter your email address = ");
        emailAddress = input.nextLine();
        System.out.println("Please enter your phone number = ");
        phoneNumber = input.nextLine();
        System.out.println("Please enter you address = ");
        address = input.nextLine();

        System.out.println("\tUser who joined the program is " + firstName + " " + lastName + " ." + firstName +
                "'s age is \n" +  age + ". " + firstName + "'s email address is " + emailAddress + ", phone number \n is "
        + phoneNumber + ", and address is " + address + ".");





    }
}

