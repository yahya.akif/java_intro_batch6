package homeworks;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class Homework07 {
    public static void main(String[] args) {
        System.out.println("\n------------------Task-1-----------------\n");

        ArrayList<Integer> list = new ArrayList<>(Arrays.asList(10,23,67,23,78));

        System.out.println(list.get(3));
        System.out.println(list.get(0));
        System.out.println(list.get(2));
        System.out.println(list);

        System.out.println("\n------------------Task-2-----------------\n");

        ArrayList <String> list2 = new ArrayList<>(Arrays.asList("Blue", "Brown", "Red", "White", "Black", "Purple"));

        System.out.println(list2.get(1));
        System.out.println(list2.get(3));
        System.out.println(list2.get(5));
        System.out.println(list2);

        System.out.println("\n------------------Task-3-----------------\n");

        ArrayList <Integer> list3 = new ArrayList<>(Arrays.asList(23, -34, -56, 0 , 89, 100));

        System.out.println(list3);

        Collections.sort(list3);
        System.out.println(list3);

        System.out.println("\n------------------Task-4-----------------\n");

        ArrayList <String> cities = new ArrayList<>(Arrays.asList("Istanbul", "Berlin", "Madrid", "Paris"));

        System.out.println(cities);

        Collections.sort(cities);
        System.out.println(cities);

        System.out.println("\n------------------Task-5-----------------\n");

        ArrayList <String> marvel = new ArrayList<>(Arrays.asList("Spider Man", "Iron Man", "Black Panther",
                "DeadPool", "Captain America"));

        System.out.println(marvel);

        if (marvel.contains("Wolverine")) System.out.println(true);
        else System.out.println(false);

        System.out.println("\n------------------Task-6-----------------\n");

        ArrayList <String> avengers = new ArrayList<>(Arrays.asList("Hulk", "Black Widow", "Captain America", "Iron Man"));

        Collections.sort(avengers);

        System.out.println(avengers);

        System.out.println(avengers.contains("Hulk") && avengers.contains("Iron Man"));

        System.out.println("\n------------------Task-7-----------------\n");

        ArrayList <Character> chars = new ArrayList <> (Arrays.asList('A','x','$', '%', '9', '*', '+', 'F', 'G'));

        System.out.println(chars);

        System.out.println(chars.get(0));
        System.out.println(chars.get(1));
        System.out.println(chars.get(2));
        System.out.println(chars.get(3));
        System.out.println(chars.get(4));
        System.out.println(chars.get(5));
        System.out.println(chars.get(6));
        System.out.println(chars.get(7));
        System.out.println(chars.get(8));

        System.out.println("\n------------------Task-8-----------------\n");

        ArrayList <String> object = new ArrayList<>(Arrays.asList("Desk", "Laptop", "Mouse", "Monitor", "Mouse-Pad", "Adapter"));

        System.out.println(object);

        Collections.sort(object);
        System.out.println(object);

        int count = 0;
        int count2 = 0;

        for (String element:object) {
            if (element.toString().startsWith("M")) count++;
            if (!element.toLowerCase().contains("a") && !element.toLowerCase().contains("e")) count2++;
        }

        System.out.println(count);
        System.out.println(count2);

        System.out.println("\n------------------Task-9-----------------\n");

        ArrayList <String> kitchenObjects = new ArrayList<>(Arrays.asList("Plate", "spoon", "fork", "Knife", "cup", "Mixer"));

        System.out.println(kitchenObjects);

        int upppercase = 0;
        int lowercase = 0;
        int hasPp = 0;
        int startsWtihPp = 0;

        for (String element:kitchenObjects) {
            if (element.toUpperCase().charAt(0) == element.charAt(0)) upppercase++;
            if (element.toLowerCase() == element) lowercase++;
            if (element.toLowerCase().contains("p")) hasPp++;
            if (element.toLowerCase().startsWith("p") || element.toLowerCase().endsWith("p")) startsWtihPp++;
        }

        System.out.println("Elements starts with uppercase = " + upppercase);
        System.out.println("Elements starts with lowercase = " + lowercase);
        System.out.println("Elements having P or p = " + hasPp);
        System.out.println("Elements starting or ending with P or p = " + startsWtihPp);


        System.out.println("\n------------------Task-10-----------------\n");

        ArrayList <Integer> numbers = new ArrayList<>(Arrays.asList(3, 5, 7, 10, 0, 20, 17, 10, 23, 56, 78));

        System.out.println(numbers);
        int divided10 = 0;
        int greater15 = 0;
        int oddLessThan20 = 0;
        int less15More50 = 0;

        for (int num:numbers) {
            if (num % 10 == 0)  divided10++;
            if (num%2 == 0 && num > 15) greater15 ++;
            if (num%2 == 1 && num < 20) oddLessThan20++;
            if (num < 15 || num > 50) less15More50++;
        }

        System.out.println("Elements that can be divided by 10 = " + divided10);
        System.out.println("Elements that are even greater than 15  = " + greater15);
        System.out.println("Elements that are odd and less than 20 = " + oddLessThan20);
        System.out.println("Elements that are less than 15 or greater than 50 = " + less15More50);


        System.out.println("\n------------------End-of-Project-----------------\n");
    }
}
