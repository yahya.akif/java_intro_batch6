package homeworks;




import utitlities.ScannerHelper;

import java.util.Arrays;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Homework08 {
    public static void main(String[] args) {
        System.out.println("---------Task-1----------");
        String str = "hello";
        System.out.println(countConsonants(str));

        String str123 = "JAVA";
        System.out.println(countConsonants(str123));

        System.out.println("---------Task-2----------");

        System.out.println(Arrays.toString(wordArray("java is  fun")));

        System.out.println(Arrays.toString(wordArray("Hello, nice to meet you!!")));

        System.out.println("---------Task-3----------");

        System.out.println(removeExtraSpace("Hello,  nice to meet   you!!"));


        System.out.println(removeExtraSpace("Java is fun"));

        System.out.println("---------Task-4----------");
        System.out.println(count3OrLess());

        System.out.println("---------Task-5---------");

        String str5 = "01/21/1999";
        System.out.println(isDateFormatValid(str5));

        String str52 = "1/20/191";
        System.out.println(isDateFormatValid(str52));

        System.out.println("---------Task-6----------");

        String str6 = "abc@gmail.com";
        System.out.println(isEmailFormatValid(str6));

        String str61 = "a@gmail.com";
        System.out.println(isEmailFormatValid(str61));

        System.out.println("------End-of-Program-----");

    }

    public static int countConsonants (String str) {
        str = str.replaceAll("[aeiouAEIOU]" ,"");

        return str.length();
        }

    public static String[] wordArray (String str){
        str = str.replaceAll("\\s+"," "); // replace anything with more than once space with only one space
        str = str.replaceAll("[!=$#@]",""); // or [^a-zA-Z
        return str.trim().split(" ");

    }

    public static String removeExtraSpace (String str){
        str = str.trim();
        str = str.replaceAll("\\s+"," ");
        return str;
//        String s = "";
//        for (int i = 0; i < str.length(); i++) {
//            if ((str.charAt(i) == ' ') && (str.charAt(i+1) == ' ')) continue;
//            else s = s + str.charAt(i);
//
//
//
//        }
//     return s;
    }

    public static int count3OrLess () {
        String str = ScannerHelper.getString();

       Pattern pattern = Pattern.compile("[a-zA-Z]{1,3} ");
       Matcher matcher = pattern.matcher(str);

        int count = 0;
        while (matcher.find()){
                count++;
        }
        return count;
    }

    public static boolean isDateFormatValid (String str) {

        return str.matches("(0?[\\d]|1[012])/(0?[1-9]|[12][0-9]|3[01])/[0-9]{4}");


    }

    public static boolean isEmailFormatValid (String str) {
        if (str == null || str.isEmpty()) {
            return false;
        }

        return  str.matches("([a-zA-Z0-9$%#!.]{2,})@([a-zA-Z_.-]{2,}.([a-zA-Z]{2,}))");

        }
    }


