package homeworks;

import utitlities.MathHelper;

import java.time.LocalDate;

import java.util.Arrays;


public class Homework11 {
    public static void main(String[] args) {
        System.out.println("-------Task-1--------");
        System.out.println(noSpace("   Hello    "));
        System.out.println(noSpace(" Hello World   "));

        System.out.println("-------Task-2--------");
        System.out.println(replaceFirstLast("A"));
        System.out.println(replaceFirstLast("Hello"));
        System.out.println(replaceFirstLast("Tech Global"));

        System.out.println("-------Task-3--------");
        System.out.println(hasVowel(""));
        System.out.println(hasVowel("1234"));
        System.out.println(hasVowel("ABC"));
        System.out.println(hasVowel("java"));

        System.out.println("-------Task-4--------");
        System.out.println(checkAge(2010));
        System.out.println(checkAge(2006));
        System.out.println(checkAge(2050));

        System.out.println("-------Task-5--------");
        System.out.println(averageOfEdges(0,0,6));
        System.out.println(averageOfEdges(-2,-2,10));
        System.out.println(averageOfEdges(10,13,20));

        System.out.println("-------Task-6--------");
        System.out.println(Arrays.toString(noA(new String[]{"appium", "123", "ABC", "java"})));
        System.out.println(Arrays.toString(noA(new String[]{"apple", "appium", "ABC", "Alex", "A"})));

        System.out.println("-------Task-7--------");
        System.out.println(Arrays.toString(no3or5(new int[]{3,4,5,6})));
        System.out.println(Arrays.toString(no3or5(new int[]{10,11,12,13,14,15})));

        System.out.println("-------Task-8--------");
        System.out.println(countPrimes(new int[]{-10,-3,0,1}));
        System.out.println(countPrimes(new int[]{7,4,11,23,17}));
        System.out.println(countPrimes(new int[]{41,53,19,47,67}));
    }


    public static String noSpace(String str){
        return str.trim().replaceAll("\\s+","");
    }

    public static String replaceFirstLast(String str){
        if (str.trim().length() < 2) return "";
        else return str.substring(str.length()-1) + str.substring(1,str.length()-1) + str.charAt(0);
    }

    public static boolean hasVowel(String str){
       String noVowel = str.replaceAll("[aeiouAEIOU]","");

        return str.length() > noVowel.length();
    }

    public static String checkAge(int yob) {
        LocalDate date = LocalDate.now();
        date.getYear();

        int age = date.getYear() - yob;
        if (age < 0 || age > 100) return "AGE IS NOT VAlID";
        else if (age < 16) return "AGE IS NOT ALLOWED";
        else return "AGE is Allowed";
    }

    public static int averageOfEdges(int a, int b, int c){
        int max = MathHelper.max(a,b,c);
        int min = Math.min(a,Math.min(b,c));
        return (max+min)/2;
    }

    public static String [] noA(String [] arr){
        String[] arr2 = new String [arr.length];
        for (int i = 0; i < arr.length; i++) {
            if (arr[i].startsWith("a") || arr[i].startsWith("A")) {
                arr2[i] = arr[i].replaceAll("[a-zA-Z1-9]","#");

            } else arr2[i] = arr[i];
        }
        return arr2;
    }

    public static int [] no3or5(int [] arr){
        int [] arr2 = new int[arr.length];
        for (int i = 0; i < arr.length; i++) {
            if (arr[i]%15 == 0) arr2[i] = 101;
            else if (arr[i]%5 == 0) arr2[i] = 99;
            else if(arr[i]%3 == 0) arr2[i] = 100;
            else arr2[i] = arr[i];

        }
        return arr2;
    }

    public static int countPrimes (int [] arr){
        int countP =0;
        for (int j : arr) {
            boolean prime = true;

            for (int y = 2; y < j; y++) {
                if (j % y == 0) {
                    prime = false;
                    break;
                }
            }

            if (prime && j > 1) {
                countP++;
            }

        }
        return countP;
    }
}
