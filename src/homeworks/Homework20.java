package homeworks;

public class Homework20 {
    public static void main(String[] args) {
        System.out.println("-----------Task-1---------");

        System.out.println(sum(new int[]{1, 5, 10}, true));
        System.out.println(sum(new int[]{3, 7, 2, 5, 10},false));

        System.out.println("-----------Task-2---------");

        System.out.println(sumDigitsDouble("Java"));
        System.out.println(sumDigitsDouble("23abc45"));
        System.out.println(sumDigitsDouble("ab12"));
        System.out.println(sumDigitsDouble("Hi-23"));

        System.out.println("-----------Task-3---------");

        System.out.println(countOccurrence("Can I can a can", "anc"));
        System.out.println(countOccurrence("IT conversations", "IT"));
        System.out.println(countOccurrence("Hello", "World"));
        System.out.println(countOccurrence("Hello", "l"));
    }

    public static int sum(int[] arr,  boolean isEven){
        int count = 0;

        for (int i = 0; i < arr.length; i++) {
            if(isEven){
                if(arr[i]%2 == 0) count++;
            }else{
                if(arr[i]%2 != 0) count++;
            }
        }return count;
    }

    public static int sumDigitsDouble(String str ) {


        int count = 0;
        char[] chars = str.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            if (Character.isDigit(chars[i])) {
                count += Character.getNumericValue(chars[i]);
            }
        }
        return count > 0 ? count * 2 : -1;
    }
    public static int countOccurrence(String str1, String str2) {
        char[] arr1 = str1.toLowerCase().toCharArray();
        char[] arr2 = str2.toLowerCase().toCharArray();
        int wordCount = 0;

        for (int i = 0; i < arr2.length; i++) {
            boolean isValid = false;
            for (int j = 0; j < arr1.length; j++) {
                if (arr1[j] == arr2[i]) {
                    isValid = true;
                    arr1[j] = ' ';
                    wordCount++;
                    break;
                }
            }
            if(!isValid)
                return 0;
        }
        return wordCount;
    }
}