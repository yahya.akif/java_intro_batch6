package recursion;

public class TestRecursion {
    public static void main(String[] args) {
       // printHello();
        System.out.println(sum5());

        System.out.println(sum1ToNRecursive(4));
        System.out.println(sum1ToNRecursive(5));
        System.out.println(sum1ToNRecursive(10));

        System.out.println(factorial(3));
        System.out.println(factorial(5));
        System.out.println(factorial(6));
    }

//    public static void printHello(){
//        System.out.println("Hello");
//        printHello();
//    }

    public static int sum5(){
        int sum =0;
        for (int i = 1; i <6 ; i++) {
            sum+=i;
        }
        return sum;
    }

    public static int sum1ToNRecursive(int n){
        if(n != 1) return n + sum1ToNRecursive(n-1);
        return 1;
    }

    public static int factorial(int n){
        if(n != 1) return n * factorial(n-1);
        return 1;
    }
}
