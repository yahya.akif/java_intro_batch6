package operator.arithmatic_operator;

public class MathFunctions {
    public static void main(String[] args) {
        int num1 = 9, num2 = 3;
        int sum = num1 + num2;
        int subtract = num1 - num2;
        int multiply = num1 * num2;
        int division = num1 / num2;
        int remainder = num1 % num2;

        System.out.println(sum);
        System.out.println(subtract);
        System.out.println(multiply);
        System.out.println(division);
        System.out.println(remainder);
    }
}
