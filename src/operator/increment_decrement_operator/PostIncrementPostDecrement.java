package operator.increment_decrement_operator;

public class PostIncrementPostDecrement {
    public static void main(String[] args) {
        int num1 = 10, num2 = 10;

        System.out.println(num1++);
        System.out.println(++num2);
        System.out.println(num1);

        System.out.println("========Task-2=========");
        int n1 = 5, n2 = 7;

        n1++;
        n1 += n2;
        System.out.println(n1);
        System.out.println("=======Task-3=====");
        int i1 = 10;

        --i1; // -1 now =9
        i1--; // decrease by 1 next line =9

        System.out.println(--i1);  // decrease by 1 then take another one now =7

        System.out.println("=======Task-3=====");
        int number1 = 50;
        number1 -= 25;
        number1 -=10;
        System.out.println(number1--);
    }
}
