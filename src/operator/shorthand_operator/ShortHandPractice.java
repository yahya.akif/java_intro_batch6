package operator.shorthand_operator;

public class ShortHandPractice {
    public static void main(String[] args) {

        int age = 45;

        System.out.println("age om 2023 = " + age);
        //age = age + 5;
        age += 5;
        System.out.println(age);
        age -= 20;
        System.out.println(age);
        age /= 3;
        System.out.println(age);
        age %= 4;
        System.out.println(age);


    }
}
