package arraylist_linkedlist_vector;

import java.util.ArrayList;
import java.util.Collections;

public class _07_ArrayList_To_Array {
    public static void main(String[] args) {

        System.out.println(uniques(new int[]{3, 5, 7, 3, 5}));
    }


    /*
    Write a method (uniques) that takes some numbers and returns the unique numbers back
     */
    public static ArrayList<Integer> uniques(int[] arr) {
        ArrayList<Integer> list = new ArrayList<>();
        for (int element : arr) {
            if (!list.contains(element)) list.add(element);
        }
        return (list);
    }
}



