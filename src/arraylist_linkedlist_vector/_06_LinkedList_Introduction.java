package arraylist_linkedlist_vector;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;

public class _06_LinkedList_Introduction {
    public static void main(String[] args) {

        LinkedList<String> cities = new LinkedList<>(Arrays.asList("Berlin", "Rome", "Kyiv", "Ankra", "Madrid", "Chicago"));

        System.out.println(cities.size()); //6
        System.out.println(cities.contains("Miami")); // false
        System.out.println(cities.indexOf("Miami")); // -1
        System.out.println(cities.getFirst());
        System.out.println(cities.getLast());

        System.out.println(cities.pop()); // remove

        cities.push("Barcelona");

        System.out.println(cities);
    }
}
