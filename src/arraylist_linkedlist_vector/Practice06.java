package arraylist_linkedlist_vector;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class Practice06 {
    public static void main(String[] args) {
        System.out.println("\n----------task-1----------\n");

        System.out.println(Arrays.toString(double1(new int [] {3,2,5,7,0})));

        System.out.println("\n----------task-2----------\n");

        ArrayList<Integer> list = new ArrayList<>(Arrays.asList(2,3,7,1,1,7,1));
        System.out.println(secondMax(list));
        System.out.println("\n----------task-3----------\n");

        ArrayList<Integer> list2 = new ArrayList<>(Arrays.asList(2,3,7,1,1,7,1));

        System.out.println(secondMin(list2));
        System.out.println("\n----------task-4----------\n");
        ArrayList<String> list4 = new ArrayList<>(Arrays.asList("Tech", "Global", "", "School", null));
        System.out.println(removeEmpty(list4));

        System.out.println("\n----------task-5----------\n");
        ArrayList<Integer> list5 = new ArrayList<>(Arrays.asList(200,5,99,101,75,-100,-80,-110));
        System.out.println(remove3orMore(list5));

        System.out.println("\n----------task-6----------\n");
        System.out.println(uniqueWords("Star Light Star Bright"));



    }

    /*
    Task01
    Method name Double
    take int and return int array
     */
    public static int [] double1 (int [] arr) {

        for (int i = 0; i < arr.length; i++) {
            arr[i] *= 2;
        }

        return arr;
    }

    /*
    2nd max of arraylist
     */
    public static int secondMax (ArrayList<Integer> list) {


        int max = Integer.MIN_VALUE;
        int secondMax = Integer.MIN_VALUE;

        for (Integer n : list)

            if (n> max) {
                secondMax = max;
                max = n;
            }

    return secondMax;
    }

    public static int secondMin (ArrayList<Integer> list2) {
        // first way-sort
        Collections.sort(list2);

        for (int i = 1; i < list2.size() ; i++) {
            if (list2.get(i) > list2.get(0)) return list2.get(i);
        }
        return 0;
        //2nd way - no sort
//        int min = Integer.MAX_VALUE;
//        int secondMin = Integer.MAX_VALUE;
//
//        for (int m: list2)
//            if (m < min) {
//                secondMin = min;
//                min = m;
//            }
//     return secondMin;
    }
    
    public static ArrayList<String> removeEmpty (ArrayList<String> list4) {
        ArrayList<String> noEmpty = new ArrayList<>();

        for (String element : list4) {

            if ((element != null)) {
                if (!element.isEmpty() ) noEmpty.add(element);
            }


                //list4.remove(element);
            }
        return noEmpty;
        }
//
//        list4.removeIf(e -> e == null || e.isEmpty());
//        return list4;
 //   }

    public static ArrayList <Integer> remove3orMore (ArrayList<Integer> list5) {
        list5.removeIf(e -> Math.abs(e) > 99);
        return list5;

    }

    public static ArrayList <String> uniqueWords (String str) {
        ArrayList <String> unique = new ArrayList<>();


        for (String s : str.split(" ")) {
            if (!unique.contains(s)) unique.add(s);
        }

        return unique;
    }
}
