package arraylist_linkedlist_vector;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class _05_Array_To_ArrayList {
    public static void main(String[] args) {

        /*
        Array ---> to an ArrayList
         */

        System.out.println("\n-------way-1 as list() method-------\n");
        ArrayList<String> cities = new ArrayList<>(Arrays.asList("Berlin", "Paris", "Rome"));

        System.out.println(cities);

        System.out.println("\n----------way-2-------\n");
        String [] countries = {"USA", "Germany", "Spain", "Italy"};

        ArrayList<String> list = new ArrayList<>();

        list.add(countries[0]);
        list.add(countries[1]);
        list.add(countries[2]);
        list.add(countries[3]);

        // for (int i = 0; i < countries.length ; i++) {
          //  list.add(countries[i]);
      //  }

        System.out.println(list);

        System.out.println("\n-------------way-3 collections.addAll() method");
        Collections.addAll(list, countries);

        System.out.println(list);
    }
}
