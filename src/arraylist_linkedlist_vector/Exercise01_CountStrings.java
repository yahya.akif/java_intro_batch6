package arraylist_linkedlist_vector;

import java.util.ArrayList;
import java.util.Arrays;

public class Exercise01_CountStrings {
    public static void main(String[] args) {
        ArrayList<String> list1 = new ArrayList<>(Arrays.asList("Hello", "Hi", "School", "Computer"));

        System.out.println(countO(list1)); //3
        System.out.println(countO(new ArrayList<>())); //0
        System.out.println(countO(new ArrayList<>(Arrays.asList("Object", "Laptop")))); //2

        System.out.println("\n-------------more 3-Method-------\n");
        System.out.println(more3(new ArrayList<>(Arrays.asList())));
        System.out.println(more3(new ArrayList<>(Arrays.asList("Hello", "Hi", "School", "Computer"))));
        System.out.println(more3(new ArrayList<>(Arrays.asList("Object", "Laptop"))));
    }
    /*
    Write method called count0 that takes an ArrayList of String it returns how many elements in the
    ArrayList contains "O" or "o"
     */

    public static int countO(ArrayList<String> list) {
        int countO = 0;

        for (String element:list) {
            if(element.toLowerCase().contains("o")) countO++;
        }
        return countO;
    }

    /*
    write a method called as more10 takes arraylist of string as argument returns
    element that has length of 3 or more
     */

    public static int more3 (ArrayList<String> list2) {
        int count2 = 0;

        for (String element:list2) {
            if (element.length() > 2) count2++;
        }
        return count2;
    }

}
