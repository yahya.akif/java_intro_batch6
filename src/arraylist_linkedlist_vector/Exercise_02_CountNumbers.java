package arraylist_linkedlist_vector;

import java.util.ArrayList;
import java.util.Arrays;

public class Exercise_02_CountNumbers {
    public static void main(String[] args) {

        System.out.println(countEven(new ArrayList<>(Arrays.asList(2, 3, 5))));
        System.out.println(countEven(new ArrayList<>(Arrays.asList(20, 30, 10))));
        System.out.println(countEven(new ArrayList<>(Arrays.asList())));
        System.out.println(countEven(new ArrayList<>(Arrays.asList(-1, 3, 17, 25))));

        System.out.println("\n-----void method------\n");

        more15(new ArrayList<>(Arrays.asList(10,5,2,-20)));
        more15(new ArrayList<>(Arrays.asList(100,200,45)));
        more15(new ArrayList<>(Arrays.asList()));

        System.out.println(no3(new ArrayList<>(Arrays.asList(13,3,30,300,533))));
        System.out.println(no3(new ArrayList<>(Arrays.asList(0,1,30,13))));
    }

    /*
    write method called as countEven takes ArrayList of Integer
     */

   // public static int countEven(ArrayList<Integer> list) {
     //   int count = 0;

    //    for (Integer number : list) {
      //      if (number % 2 == 0) count++;
      //  }
      //  return count;
   // }

    public static int countEven(ArrayList<Integer> list) {
        return (int) list.stream().filter(element ->element % 2 == 0).count();  // Streams
    }

    /*
    write a method called more 15 takes an array list of integer print how many are more than 15
     */

//    public static void more15 (ArrayList<Integer> list) {
//        int count = 0;
//
//        for (Integer integer: list) {
//            if (integer > 15) count++;
//        }
//        System.out.println(count);
//    }

    public static void more15 (ArrayList<Integer> list) {
        System.out.println(list.stream().filter(x ->x >15).count());
    }

    // write a method called no3 counts all numbers that has a 3 digit inside the number it returns total count of elements
//    public static int no3 (ArrayList<Integer> list) {
//        int count = 0;
//        for (Integer integer :list) {
//
//            if(integer.toString().contains("3")) count++;
//        }
//        return count++;
//    }
    public static int no3 (ArrayList<Integer> list) {
        return (int) list.stream().filter(y -> y.toString().contains("3")).count();

    }
}
