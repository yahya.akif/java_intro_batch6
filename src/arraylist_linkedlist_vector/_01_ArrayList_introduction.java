package arraylist_linkedlist_vector;

import com.sun.corba.se.spi.orbutil.threadpool.NoSuchThreadPoolException;

import java.util.ArrayList;
import java.util.Arrays;

public class _01_ArrayList_introduction {
    public static void main(String[] args) {
         // how to create an array vs arraylist

        String [] array = new String[3];

        ArrayList<String> list = new ArrayList<>(); // capacity = 10 by default

        //2. how to get size of an array and arraylist

        System.out.println("The size of the array = " + array.length); // 3
        System.out.println("The size of the list = " + list.size()); // 0

        //3. how to print an Array vs Arraylist

        System.out.println("The array = " + Arrays.toString(array));
        System.out.println("The list = " + list);

        //4. How to add elements to anArray vs ArrayList

        array [1] = "Alex";
        array [2] = "MAx";
        array [0] = "John";
        System.out.println(array[0]);
        System.out.println("The array = " + Arrays.toString(array));

        list.add("Joe");
        list.add("Jane");
        list.add("Mike");
        list.add("Adam");

        list.add(2,"Jazzy");
        // list.add(20, "Yahya"); out of bounds exception
        System.out.println("The size of the list = " + list); //[joe,jane,Mike,Adam]

        //5. how to update an existing element in an Array vs Arraylist

        array [1] = "Ali";
        System.out.println("The size of the array = " + Arrays.toString(array));

        list.set(1, "Jasmine");
        System.out.println("The list = " + list);

        //6. how to get an element from Array vs ArrayList

        System.out.println(array[2]); //Max

        System.out.println(list.get(3)); // Mike


        //7. how to loop an Array vs ArrayList

        System.out.println("\n-----Task-7 for i loop");
        for (int i = 0; i < array.length; i++) {
            System.out.println(array[i]);
        }

        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }

        System.out.println("\n-----Task-7 for each loop");
        for (String element:array) {
            System.out.println(element);
        }

        for (String element: list) {
            System.out.println(element);
        }

        System.out.println("\n------For each-new way---------\n");
        list.forEach(System.out::println);  // lambda expression
    }
}
