package utitlities;

import java.util.Random;

public class MathHelper {

    public static int max(int num1, int num2, int num3){
        return Math.max(Math.max(num1,num2),num3);
    }

    public static int sum(int a, int b){
        return (a+b);
    }
    public static int sum(int a, int b, int c) {
        return (a + b + c);
    }
    public static double sum(double a, double b) {
        return (a + b);
    }
    public static long sum(long a, long b) {
        return (a + b);
    }
    public static int product(int a, int b){
        return (a * b);
    }

    public static double sqrt(double num1) {
        return (double) Math.sqrt(num1);
    }


}
