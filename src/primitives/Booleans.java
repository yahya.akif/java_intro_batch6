package primitives;

public class Booleans {
    public static void main(String[] args) {
        /*
        boolean -> 1 bit
         */
        boolean b1 = true;
        boolean b2 = false;

        System.out.println(b1);
        System.out.println(b2);

    }
}
