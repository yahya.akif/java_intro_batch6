package primitives;

public class Characters {
    public static void main(String[] args) {
        /*
        char  --> 2 bytes
        Single character storage
         */
        char c1 = 'A';
        char c2 = 'X';


        System.out.println(c1);
        System.out.println(c2);

        char myFavCharacter = 'P';
        System.out.println(myFavCharacter);
    }
}
