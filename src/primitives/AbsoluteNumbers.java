package primitives;

public class AbsoluteNumbers {
    public static void main(String[] args) {
        /*
        byte --> -128,127
        Short --> -32768,32767
        int --> -2m,2m
        long -->

        dataType variable = value

         */
        System.out.println("\n---------byte------\n");
        byte myNumber = 45;

        System.out.println("the max value of byte = " + Byte.MAX_VALUE); // 127
        System.out.println("the min value of byte = " + Byte.MIN_VALUE); // -128

        System.out.println("\n--------short-----\n");
        short numberShort = 150;
        System.out.println(numberShort);
        System.out.println("The max value of short = " + Short.MAX_VALUE); // 32767
        System.out.println("The min value of short = " + Short.MIN_VALUE); // -32768

        System.out.println("\n------Int----\n");
        int myInteger = 2000000;
        System.out.println("The max value of Integer = " + Integer.MAX_VALUE);
        System.out.println("The min value of Integer = " + Integer.MIN_VALUE); // -2147483648


        System.out.println(myInteger);

        System.out.println("\n------long-----\n");
        long myBigNumber = 2147483648L;

        System.out.println(myBigNumber); //2147483648

        long l1= 5;

        System.out.println(l1);


    }
}
