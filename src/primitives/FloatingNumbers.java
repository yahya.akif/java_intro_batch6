package primitives;

public class FloatingNumbers {
    public static void main(String[] args) {
        /*
        Floating numbers: 10.5
        float -> 4 bytes
        double -> 8 bytes

         */
        System.out.println("\n------floating numbers----\n");
        float myFloat = 20.5F;
        double myDouble = 20.5;

        System.out.println(myFloat);
        System.out.println(myDouble);

        System.out.println("\n------floating numbers more----\n");
        float myFloat2 = 10;
        double myDouble2 = 234235;

        System.out.println(myFloat2); // 10.0
        System.out.println(myDouble2); // 234235.0

        System.out.println("\n------floating numbers precision----\n");

        float f1 = 3274.63272335645646F;
        double d1 = 3274.63272335645646;

        System.out.println(f1);
        System.out.println(d1);



    }
}
