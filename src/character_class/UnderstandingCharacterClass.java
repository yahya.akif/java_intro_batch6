package character_class;

import utitlities.ScannerHelper;

public class UnderstandingCharacterClass {
    public static void main(String[] args) {
        /*
        char is primitive data type
        character is a wrapper class
         */
        String str = ScannerHelper.getString();

       char firstChar = str.charAt(0);
    //    if (firstChar >= 65 && firstChar <= 90)
        //   System.out.println("True");
    //    } else System.out.println("false");

        // character method
        System.out.println(Character.isUpperCase(firstChar));
        System.out.println(Character.isLowerCase(firstChar));
        System.out.println(Character.isDigit(firstChar));
        System.out.println(Character.isLetter(firstChar));
        System.out.println(Character.isLetterOrDigit(firstChar));
        System.out.println(Character.isWhitespace(firstChar));
        System.out.println(Character.isSpaceChar(firstChar));
}}
