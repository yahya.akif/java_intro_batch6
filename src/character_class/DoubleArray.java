package character_class;

import java.util.Arrays;

public class DoubleArray {
    public static void main(String[] args) {

        System.out.println("\n-------------Task-1-----------------");

        double [] numbers = {5.5,6,10.3,25};
        System.out.println("\n-------------Task-2-----------------");

        System.out.println(Arrays.toString(numbers));

        System.out.println("\n-------------Task-3-----------------");

        System.out.println("The length is " + numbers.length);

        System.out.println("\n-------------Task-4-----------------");

        for (double num: numbers) {
            System.out.println(num);
        }


        System.out.println("\n-------------End-----------------");



    }
}
