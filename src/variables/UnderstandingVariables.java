package variables;

public class UnderstandingVariables {
    public static void main(String[] args) {
       String name = "John"; // declaring and initializing the variable
        name = "joan";

        System.out.println(name);
        int age = 25;
        System.out.println(age);

        double d1 = 10;
        System.out.println(d1);

        d1 = 15.5;
        System.out.println(d1);

        d1 = 23.53;
        System.out.println(d1);
        System.out.println(d1);
        System.out.println(d1);
    }
}
