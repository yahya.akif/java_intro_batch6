package variables;

public class CreatingMultipleVariables {
    public static void main(String[] args) {
        int age1;
        int age2;
        int age3;

        int age4,age5,age6;
        age4 = 15;
        age6 = 30;
        System.out.println(age4);
        System.out.println(age6);
        // System.out.println(age5); -> compile error because age5 is not initialized

        double d1;

    }
}
