package loops;

public class Exercise11_FizzBuzz {
    public static void main(String[] args) {
        /*
        print 1-30 (included) if divisible by 3 "Fizz" %% divisile by 5 "Buzz"
         */
        int n = 1;

        while (n<= 30) {
            if (n % 3 == 0 && n % 5 == 0) {
                System.out.println("FizzBuzz");
            } else if (n % 3 == 0) {
                System.out.println("Fizz");
            } else if (n % 5 == 0) {
                System.out.println("buzz");
            } else System.out.println(n);
            n++;
        }}}
