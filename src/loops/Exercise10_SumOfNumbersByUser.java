package loops;

import utitlities.ScannerHelper;

public class Exercise10_SumOfNumbersByUser {

    public static void main(String[] args) {
        /*
        int num = ScannerHelper.getNumber();
        int num2 = ScannerHelper.getNumber();
        int num3 = ScannerHelper.getNumber();
        int num4 = ScannerHelper.getNumber();
        int num5 = ScannerHelper.getNumber();


        System.out.println(num+num2+num3+num4+num5);
*/
        //2nd way
        System.out.println("======Task2======");
        int sum = 0;

        for (int i = 1; i <= 5; i++) {
            sum += ScannerHelper.getNumber();
        }
        System.out.println(sum);


        //3rd way
        System.out.println("=========task-3==============");
        int n1 = 1;
        int sumWhile = 0;

        while (n1 <= 5) {
            sumWhile += ScannerHelper.getNumber();
            n1++;
        }
        System.out.println(sumWhile);
    }
}
