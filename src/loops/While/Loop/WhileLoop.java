package loops.While.Loop;

public class WhileLoop {
    public static void main(String[] args) {

        System.out.println("\n=======forLoop===========\n");

        for (int i = 1; i <=5; i++) {
            System.out.println(i);
        }


        System.out.println("\n=======WhileLoop===========\n");
        int num =1;

        while (num <= 5) {
            System.out.println(num);
            num++;
        }



}}
