package loops.While.Loop;

public class Exercise01_PrintNumbersDivideBy3 {
    public static void main(String[] args) {
        /*
        print all num divided by 3 (1 to 100 included)
         */

        int num = 1;

        while (num <= 100) {
            if (num%3 == 0) System.out.println(num);
            num++;
        }
    }
}
