package loops.do_while_loop;

public class DoWhileLoop {
    public static void main(String[] args) {
        int num = 0;

        do {
            System.out.println(num);   // execute at least once then checks
            num++;
    }
        while (num<=10);
        System.out.println("End of the program");

        int start = 0;

        while (start <= -1) {
            System.out.println(start);
            start++;
        }


}}
