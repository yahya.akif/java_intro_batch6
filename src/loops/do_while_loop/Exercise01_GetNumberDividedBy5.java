package loops.do_while_loop;

import utitlities.ScannerHelper;

public class Exercise01_GetNumberDividedBy5 {
    public static void main(String[] args) {
        int n = ScannerHelper.getNumber();
            do {
                n = ScannerHelper.getNumber(); // execute at least once then checks

            }
            while (n %5 != 0);
            System.out.println("End of the program");

            // while (true) {
            // if (n1 % 5 == 0) {
            // System.out.println("End of Program");
            // break;


            }


        }

