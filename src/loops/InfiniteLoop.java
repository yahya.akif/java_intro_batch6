package loops;

public class InfiniteLoop {
    public static void main(String[] args) {

        for (; ;) {
            System.out.println("Hello");  /// never ending loop if you run it (no termination point)
        }
    }
}
