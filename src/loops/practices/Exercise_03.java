package loops.practices;

import utitlities.ScannerHelper;

public class Exercise_03 {
    public static void main(String[] args) {
      int n1 = ScannerHelper.getNumber();
      int n2 = ScannerHelper.getNumber();

        for (int i = Math.min(n1,n2); i <= Math.max(n1,n2); i++) {
            if (i != 5) System.out.println(i);   // if (i == 5) continue;
                                                // else sout (i);
            }
        }

    }

