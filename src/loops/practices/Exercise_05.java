package loops.practices;

public class Exercise_05 {

        public static void main(String[] args) {  ///faboooneeechi
            int n1 = 5;
            int sum;

            int numPrev1 = 0;
            int numPrev2 = 1;

            System.out.print(numPrev1 + " - " + numPrev2);
            for (int i = 2; i < n1; i++) {
                sum = numPrev1 + numPrev2;
                numPrev1 = numPrev2;
                numPrev2 = sum;
                System.out.print( " - " + sum);
            }

        }}
