package loops.practices;

import utitlities.ScannerHelper;

public class Exercise_04 {
    public static void main(String[] args) {

        String str = ScannerHelper.getString();

        int vowelCount = 0;
        char c;

        for (int i = 0; i < str.length(); i++) {
            c = str.toLowerCase().charAt(i);
            if (c == 'a' || c == 'e' || c == 'i' || c == 'o' || c== 'u') {
                vowelCount++;
            }
        }

        System.out.println("There are " + vowelCount + " vowels");
    }
}
