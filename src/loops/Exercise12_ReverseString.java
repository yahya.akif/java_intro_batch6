package loops;

import utitlities.ScannerHelper;

public class Exercise12_ReverseString {
    public static void main(String[] args) {
        String word = ScannerHelper.getString();

       // for (int c = word.length()-1; c >= 0 ; c--) {

        //    System.out.print(word.charAt(c)); // way-1

         // 2nd way
            String reverseName = " ";

            for (int i = word.length()-1; i >= 0; i--) {
                reverseName += word.charAt(i);
            }

            System.out.println(reverseName);
        }
    }

