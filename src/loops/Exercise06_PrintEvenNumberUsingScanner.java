package loops;

import utitlities.MathHelper;
import utitlities.ScannerHelper;

public class Exercise06_PrintEvenNumberUsingScanner {
    public static void main(String[] args) {
        int input1 = ScannerHelper.getNumber();
        int input2 = ScannerHelper.getNumber();

        for (int i = (Math.min(input1,input2)); i <= (Math.max(input1,input2)) ; i++) {

            if (i%2 == 0) {
                System.out.println(i);;
        }
    }
}}
