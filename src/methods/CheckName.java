package methods;

import utitlities.ScannerHelper;

public class CheckName {
    public static void main(String[] args) {
        // ScannerHelper scannerHelper = new ScannerHelper(); for non-static

       // String name = scannerHelper.getFirstName();
        String name = ScannerHelper.getFirstName();
       // System.out.println("The name entered by user = " + name); you don't need

        String lastName = ScannerHelper.getLastName();
        //System.out.println("The name entered by user = " + lastName);  you don't need

        System.out.println("my name is " + name + " " + lastName);


        int age = ScannerHelper.getAge();
        int num = ScannerHelper.getNumber();

        System.out.println("Age will be " + (age + num) + " after " + num + " years.");

    }
}
