package enums;

public class Practice01 {
    public static void main(String[] args) {
        /*
        write a program that prints "it is a work day
        weekend "it is OFF today"
         */

        DaysOfTheWeek dayByUser = DaysOfTheWeek.SUNDAY;
        switch (dayByUser){
            case SUNDAY:
            case SATURDAY:
                System.out.println("It is OFF today");
                break;
            case MONDAY:
            case THURSDAY:
            case WEDNESDAY:
            case TUESDAY:
            case FRIDAY:
                System.out.println("It is work day");
                break;
            default:
                throw new RuntimeException("No such enum value!!!");
        }
    }
}
