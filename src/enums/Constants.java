package enums;

public class Constants {

    public enum  Gender{
        FEMALE,
        MALE,
        OTHER
    }

    public enum TShirtSize{
        X_SMALL,
        SMALL,
        MEDIUM,
        LARGE,
        X_LARGE
    }

    public enum Direction{
        NORTH,
        SOUTH,
        EAST,
        WEST
    }
}
