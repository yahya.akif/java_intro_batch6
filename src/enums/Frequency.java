package enums;

public enum Frequency {
    BI_WEEKLY,
    DAILY,
    Hourly,
    MONTHLY,
    WEEKLY,
    YEARLY
}
