package scanner_class;

import java.util.Scanner;

public class Recap01 {
    public static void main(String[] args) {
        /*
        write a program that asks user to enter their firstName, address, favNumber
         */
        Scanner input = new Scanner (System.in);
        String fName, address;
        int favNumber;

        System.out.println("First Name: ");
        fName = input.next();

        input.nextLine();
        System.out.println("address");
        address = input.nextLine();

        System.out.println("Your favourite number");
        favNumber = input.nextInt();

        System.out.println(fName + "'s " + "address is " + address + " and their fav number is " + favNumber );

    }
}
