package scanner_class;

import java.util.Scanner;

public class ScannerFirstAndLastName {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String fName, lName;
        System.out.println("Please enter your first name:");

        fName = scanner.next();

        System.out.println("Please enter your last name");
        lName = scanner.next();
        scanner.nextLine();
        
        System.out.println("Please enter your address.");
        String address = scanner.nextLine();

        System.out.println("your full name is " + fName + " " + lName);
        System.out.println("address: " + address);

        //nextInt method

        System.out.println("Please enter a number");
        int number = scanner.nextInt();
        System.out.println("The number you chose is: " + number);




    }






}
