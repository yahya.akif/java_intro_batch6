package casting;

public class StringToPrimitives {
    public static void main(String[] args) {
        int num1 = 5;
        int num2 = 10;

        System.out.println(num2 + num1 + 5); // all the numbers stay as Integers
        System.out.println("" + num1 + num2);
        System.out.println("" + (num1 + num2) + 5); // concatenation causes everything to be a string
        // after the quotations

        // Converting numbers to String
        System.out.println("" + num1 + num2);
        System.out.println(String.valueOf(num1) + String.valueOf(num2));

        System.out.println(String.valueOf(num2 + num1) + (7 + num2));

        // Converting String to primitives
        String price = "1597.06";

        System.out.println(Double.parseDouble(price)- 10.0);
    }
}
