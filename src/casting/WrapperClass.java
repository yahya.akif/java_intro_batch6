package casting;

public class WrapperClass {
    public static void main(String[] args) {

        int age1 = 45;
        Integer age2 = 45;

        System.out.println(age2 + 5);
        System.out.println(age1 + 5);

        System.out.println(Integer.MAX_VALUE);
        System.out.println(Double.MIN_VALUE);
        System.out.println(Short.MAX_VALUE);

        String s = "25.5";
        System.out.println(Double.parseDouble(s) + 5);
        System.out.println((int) Double.parseDouble(s) + 5);

        char c1 = 'A';
        Character c2 = c1;


    }
}
