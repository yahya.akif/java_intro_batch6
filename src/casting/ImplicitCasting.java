package casting;

public class ImplicitCasting {
    public static void main(String[] args) {
        /*
        Implicit casting is storing smaller data type into bigger data type
        happens automatically as bigger data can type can definitely hande the smaller data type
        byte --> short
        short--> Int
        float -> double
         */

        short num1 = 45;
        int num2 = num1;

        float decimal = 34.5F;
        double decimal2 = decimal;

        System.out.println(num2);
        System.out.println(decimal2);

    }
}
