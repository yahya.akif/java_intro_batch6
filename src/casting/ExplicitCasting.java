package casting;

public class ExplicitCasting {
    public static void main(String[] args) {
        /*
        storing bigger data type into smaller data type
         */

        long number1 = 273645273;
        byte number2 = (byte) number1;

        System.out.println(number2);

    }
}
