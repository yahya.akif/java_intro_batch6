package casting;

public class CastingCharacters {
    public static void main(String[] args) {
        int i1 = 65;

        char c1 = (char) i1;
        System.out.println(c1);

        char c2 = 97;
        System.out.println(c2);

        char c3 = 12;
        System.out.println(c3);
    }
}
