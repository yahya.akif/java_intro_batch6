package strings;

public class UnderstandingStrings {
    public static void main(String[] args) {
        String s1; // declaration of s1 as a string

        s1 = "TechGlobal School"; //intializing s1 as TechGlobal School
        String s2 = "is the best"; //declaration of s2 and intialization of s2 as the best;

        System.out.println("\n-----concat using +------");

        String s3 = s1 + " " + s2; // concatenation using plus sign
        System.out.println(s3); //TechGlobal school is the best

        System.out.println("\n-------concat using method-----\n");

        String s4 = s1.concat(s2); // concatenation using method
        System.out.println(s4);

        System.out.println("\n========= Practice YAYA======\n");

        String z1 = "Yahya Is a Football fan ";
        String z2 = "His favourite team is Real Madrid";
        String z3 = z1 + "$" + z2;

        String z4 = z1.concat(z2);

        System.out.println(z3);
        System.out.println(z4);



        System.out.println("\n=======EX1=====\n");

        String wordPart1 = "le";
        String wordPart2 = "ar";
        String wordPart3 = "ning";

        String fullWord = wordPart1 + wordPart2 + wordPart3;

        System.out.println(fullWord);

    }
}
