package strings;

public class Excercise01 {
    public static void main(String[] args) {
        /*
        String is a reference type (object) used to store a sequence of characters
        TEXTS
         */
        String name = "John";
        String address = "Chicago IL 12345";

        System.out.println(name);
        System.out.println(address);

        String favMovie = "Borat";

        System.out.println("My Favorite movie = " + favMovie);
    }
}
