package collections;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeSet;

public class Exercise03_CalculateCharacters {
    public static void main(String[] args) {
        /*
        given a string "Banana print all duplicate letters
         */

        System.out.println("\n----task-1-----\n");

        String str = "banana";

        HashMap<Character, Integer> lettersMap = new HashMap<>();

        for (char letter: str.toCharArray()){
            if(!lettersMap.containsKey(letter)) lettersMap.put(letter,1);
            else lettersMap.put(letter, lettersMap.get(letter)+1);
        }

        System.out.println(lettersMap);

        for (Map.Entry<Character,Integer> entry:lettersMap.entrySet()) {
            if(entry.getValue() > 1) System.out.println(entry.getKey());
        }

        System.out.println("\n------Task-2-----\n");
        String word = "pineapple";

        Map<Character,Integer> characters = new HashMap<>();

        for (int i = 0; i < word.length(); i++) {
            char currentChar = word.charAt(i);
            if(characters.containsKey(currentChar)) characters.put(currentChar,characters.get(currentChar) +1);
            else characters.put(currentChar,1);
        }
        System.out.println(characters);

        System.out.println(characters.values());

        int maxCount = new TreeSet<>(characters.values()).last();
        System.out.println(maxCount); //3

        for (Map.Entry<Character,Integer> entry: characters.entrySet()){
            if(entry.getValue() ==maxCount) {
                System.out.println("Character \"" + entry.getKey() + "\" is the most counted with " + entry.getValue() + " times.");
                break;
            }
        }
    }
}
