package collections;

import java.util.*;

public class _04_Map_Methods {
    public static void main(String[] args) {


    /*
    create a map for countries and their capitals
     */

        HashMap<String, String> capitals = new HashMap<>();

        // how to add entries to a map
        capitals.put("France", "Paris");
        capitals.put("Italy", "Rome");
        capitals.put("Spain", "Madrid");

        //how to print a map
        System.out.println(capitals);

        System.out.println("\n------How to retrieve a value----\n");
        System.out.println(capitals.get("France"));
        System.out.println(capitals.get("Italy"));
        System.out.println(capitals.get("Spain"));

        System.out.println(capitals.get("USA")); //Null

        System.out.println("\n----How to check if it contains given key or value");
        System.out.println(capitals.containsKey("Italy")); //true
        System.out.println(capitals.containsKey("Belgium")); //false

        System.out.println(capitals.containsValue("Rome")); //True
        System.out.println(capitals.containsValue("madrid")); //f -cuz LowerCase

        System.out.println("\n----How to remove some entries-----\n");
        System.out.println(capitals.remove("USA")); //Null
        System.out.println(capitals.remove("Italy","rome")); //false
        System.out.println(capitals.remove("Italy"));
        System.out.println(capitals.remove("Spain", "Madrid"));

        System.out.println(capitals);

        System.out.println("\n------size of map---\n");
        System.out.println(capitals.size());

        capitals.put("Ukraine","Kiev");
        capitals.put("Turkey","Ankra");

        System.out.println(capitals.size()); //3

        System.out.println(capitals.values()); // all values

        Collection<String> values = capitals.values();

        for (String value:values) {
            System.out.println(value);
        }
        System.out.println(capitals.keySet()); // all keys

        System.out.println("\n---How to get all entries---\n");
        System.out.println(capitals.entrySet());

        Set<Map.Entry<String,String>> entries = capitals.entrySet();

        for (Map.Entry<String,String> entry:entries){
            System.out.println(entry.getKey());
            System.out.println(entry.getValue());
        }
    }
}
