package collections;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.NoSuchElementException;
import java.util.TreeSet;

public class Exercise02_FindMinMax {
    public static void main(String[] args) {
        System.out.println(max(new Integer[]{10, 100, 123, 507, 25}));

        System.out.println(secondMax(new Integer[]{10,100,123,507,25}));
        System.out.println(secondMin(new Integer[]{10,100,123,507,25}));
    }
    /*
    write method called as max, takes numbers as argument and return max num
     */

    public static int max(Integer[] numbers){
        Arrays.sort(numbers);

       // return numbers[numbers.length-1];
        return new TreeSet<>(Arrays.asList(numbers)).last();
    }

    public static int secondMin(Integer[] numbers){
        if(numbers.length < 2) throw new NoSuchElementException("Size is " + numbers.length);
        else {
           return new ArrayList<>(new TreeSet<>(Arrays.asList(numbers))).get(1);
        }
    }

    public static int secondMax(Integer[] numbers){
        if(numbers.length < 2) throw new NoSuchElementException("Size is " + numbers.length);
        else {
            TreeSet<Integer> set = new TreeSet<>(Arrays.asList(numbers));
            set.pollLast(); // removes last
            return set.last();
        }
    }
}
