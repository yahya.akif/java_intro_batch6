package collections;

import java.util.HashMap;

public class Exercise04_ProductPrices {
    public static void main(String[] args) {
        /*
        product    price
        iphone     $1000
        Mackbook   $1300
        iMac       $1500
        Airpods    $200
        iPad       $700

        task1- find most expensive
        imac is most expensive with $1500
        task2- find most affordable
        expected airpods is most affordable with $200
         */

        HashMap<String,Double> products = new HashMap<>();
        products.put("iPhone", 1000.0);
        products.put("Macbook Pro", 1300.0);
        products.put("iMac", 1500.0);
        products.put("AirPods", 200.0);
        products.put("iPad", 700.0);

    }
}
