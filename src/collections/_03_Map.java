package collections;

import java.util.*;

public class _03_Map {
    public static void main(String[] args) {

        System.out.println("\n-----HashMap------");
        HashMap<String,Integer> hashMap = new HashMap<>();
        hashMap.put("Sandina", 25);
        hashMap.put("Zel",20);
        hashMap.put("Assem",27);
        hashMap.put("Okan",18);
        hashMap.put(null,18);
        hashMap.put(null,100);
        hashMap.put("",null);
        hashMap.put("Anton",null);
        hashMap.put("Jazzy",null);

        System.out.println(hashMap);
        System.out.println(hashMap.size()); //8

        System.out.println("\n------LinkedHashMap------\n");
        LinkedHashMap<String,Integer> linkedHashMap = new LinkedHashMap<>();

        linkedHashMap.put("Sandina", 25);
        linkedHashMap.put("Zel",20);
        linkedHashMap.put("Assem",27);
        linkedHashMap.put("Okan",18);
        linkedHashMap.put(null,18);
        linkedHashMap.put(null,100);
        linkedHashMap.put("",null);
        linkedHashMap.put("Anton",null);
        linkedHashMap.put("Jazzy",null);

        System.out.println(linkedHashMap);

        System.out.println("\n-----TreeMap------\n");
        TreeMap<String,Integer> treeMap = new TreeMap<>();
        treeMap.put("Sandina", 25);
        treeMap.put("Zel",20);
        treeMap.put("Assem",27);
        treeMap.put("Assem",24);
        treeMap.put("Anton",null);
        treeMap.put("Jazzy",null);

        System.out.println(treeMap);

        System.out.println(treeMap.descendingMap());
        System.out.println(treeMap.firstKey());
        System.out.println(treeMap.lastKey());
        System.out.println(treeMap.firstEntry());
        System.out.println(treeMap.lastEntry());

        System.out.println("\n-----Hashtable---\n");
        Hashtable<String,Integer> hashtable = new Hashtable<>();
        hashtable.put("Adam",20);
        //hashtable.put(null,25); //null pointer exception
        hashtable.put("Yana",null);

        System.out.println(hashtable);

        System.out.println("\n------HashMap,LinkedHashMap,TreeMap,HashTable---\n");
        Map<Integer,Integer> numbers1 = new HashMap<>();
        Map<Integer,Integer> numbers2 = new LinkedHashMap<>();
        Map<Integer,Integer> numbers3 = new TreeMap<>();
        Map<Integer,Integer> numbers4 = new Hashtable<>();
    }
}
