package conditional_statements;

public class IfElseSyntax {
    public static void main(String[] args) {





        boolean condition = false;

        if (condition){ // if true print A
            System.out.println("A");

        }
        else { // else block is for executing when condition is false
            System.out.println("B");

        }

        System.out.println("End of program!");
    }
}
