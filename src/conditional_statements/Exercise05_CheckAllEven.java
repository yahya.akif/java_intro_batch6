package conditional_statements;

import java.util.Scanner;

public class Exercise05_CheckAllEven {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("Please enter number 3");

        int n1 = input.nextInt();
        int n2 = input.nextInt();
        int n3 = input.nextInt();

        // System.out.println(n1 % 2 == 0 && n2 % 2 == 0 && n3 % 2 == 0);  Another way to simplify



      if (n1 % 2 == 0 && n2 % 2 == 0 && n3 % 2 == 0) {
            System.out.println("True");
        } else {
            System.out.println("False");
    }

}}
