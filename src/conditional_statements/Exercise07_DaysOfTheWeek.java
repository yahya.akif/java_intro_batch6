package conditional_statements;

import java.util.Random;

public class Exercise07_DaysOfTheWeek {
    public static void main(String[] args) {
        Random r = new Random();
        int n1 = r.nextInt(7);

       /* if (n1 == 0) {
            System.out.println("Sunday");
        } else if (n1 == 1) {
            System.out.println("Monday");
        } else if (n1 == 2) {
            System.out.println("Tuesday");
        } else if (n1 == 3) {
            System.out.println("Wednesday");
        } else if (n1 == 4) {
            System.out.println("Thursday");
             System.out.println("Friday");
        } else if (n1 == 6) {
           System.out.println("Saturday");

            */
        // switch statements

        switch (n1) {
            case 0:
                System.out.println("Sunday");
                break;
            case 1:
                System.out.println("Monday");
                break;

            case 2:
                System.out.println("Tuesday");
                break;
            case 3:
                System.out.println("wednesday");
                break;
            case 4:
                System.out.println("Thursday");
                break;
            case 5:
                System.out.println("Friday");
                break;
            default:
                System.out.println("Saturday");
    }

}}
