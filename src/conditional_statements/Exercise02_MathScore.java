package conditional_statements;

import java.util.Scanner;

public class Exercise02_MathScore {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("Please enter your grade on the exam");
        int score = input.nextInt();

        if (score >= 60) {
            System.out.println("Awesome! you have passed the math class!");
    } else {
            System.out.println("Sorry! you failed!"); }

            System.out.println("end of program");
}}
