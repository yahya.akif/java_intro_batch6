package conditional_statements;

import java.util.Scanner;

public class Exercise06_CheckAll10 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        int n1 = (int) (Math.random() * 2) + 10;
        int n2 = (int) (Math.random() * 2) + 10;

        System.out.println(n1);
        System.out.println(n2);


        if (n1 == 10 && n2 ==10) {
            System.out.println("True");
        }else {
            System.out.println("False");

    }
}}
