package conditional_statements;

import java.util.Scanner;

public class SwitchPractice {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        /*
        System.out.println("Please enter a number from 1-12");
        int num = input.nextInt();

        if (num == 1) {
            System.out.println("Jan");
        } else if (num == 2) {
            System.out.println("Feb");
        } else if (num == 3) {
            System.out.println("Mar");
        } else if (num == 4) {
            System.out.println("Apr");
        } else if (num == 5) {
            System.out.println("May");
        } else if (num == 6) {
            System.out.println("June");
        } else if (num == 7) {
            System.out.println("July");
        } else if (num == 8) {
            System.out.println("Aug");
        } else if (num == 9) {
            System.out.println("Sept");
        } else if (num == 10) {
            System.out.println("Oct");
        } else if (num == 11) {
            System.out.println("Nov");
        } else if (num == 12) {
            System.out.println("Dec");
        } else {
            System.out.println("This is not a number from 1-12");

        }
        switch (num) {

            case 1: {
                System.out.println("Jan");
                break;
            }
            case 2: {
                System.out.println("Feb");
                break;
            }
            case 3: {
                System.out.println("Mar");
                break;
            }
            case 4: {
                System.out.println("Apr");
                break;
            }
            case 5: {
                System.out.println("May");
                break;
            }
            case 6: {
                System.out.println("Jun");
                break;
            }
            case 7: {
                System.out.println("July");
                break;
            }
            default: {
                System.out.println("It's Winter");
            }
                */

            System.out.println("Please enter a, b, c");
            String letter = input.next();
          /*
            if (letter == 'a') {
                System.out.println("You entered the letter a");
            } else if (letter == 'b') {
                System.out.println("You entered the letter b");
            } else if (letter == 'c') {
                System.out.println("You entered the letter b");
            } else
                System.out.println("Error! this is not the letter a, b, or c");

            }
        }}  */

            switch (letter) {
                case "a": {
                    System.out.println("The letter you entered is a");
                    break;
                }
                case "b": {
                    System.out.println("The letter you entered is b");
                    break;
                }
                case "c": {
                    System.out.println("The letter you entered is c");
                    break;
                }
                default: {
                    System.out.println("The letter you entered is not a, b, or c");
                }
            }


        }}