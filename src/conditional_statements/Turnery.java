package conditional_statements;

import java.util.Scanner;

public class Turnery {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);


        int num1 = 51;
        int num2 = 100;
        int difference;

        difference = num1 > num2 ? num1 - num2 : num2 - num1;
        System.out.println(difference);

        System.out.println("Please enter a number");
        int number = input.nextInt();

        System.out.println((number %2 != 1) ? ("even") : ("odd"));
    }
}
