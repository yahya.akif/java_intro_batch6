package conditional_statements;

import java.util.Scanner;

public class SwitchExercise {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("Please enter a number between 1-7");
        int number = input.nextInt();


        switch (number) {
            case 1: {
                System.out.println("1st day of the week");
                break;
            }
            case 2: {
                System.out.println("2nd day of the week");
                break;
            }
            case 3: {
                System.out.println("3rd day of the week");
                break;
            }
            case 4: {
                System.out.println("4th day of the week");
                break;
            }
            case 5: {
                System.out.println("5th day of the week");
                break;
            }
            case 6: {
                System.out.println("6th day of the week");
                break;
            }
            case 7: {
                System.out.println("7th day of the week");
                break;
            }
            default: {
                System.out.println("You entered an invalid number");
            }

        }
    }
}
