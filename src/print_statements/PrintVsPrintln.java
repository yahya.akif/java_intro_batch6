package print_statements;

public class PrintVsPrintln {
    public static void main(String[] args){
        //I will write code statements here

        //Println Method
        System.out.println("Hello world");
        System.out.println("Today is Sunday");
        System.out.println("John Doe");

        //print Method
        System.out.print("Hello world");
        System.out.print("Today is Sunday");
        System.out.print("John Doe");



    }
}
