package escape_sequences;

public class Excercise01 {
    public static void main(String[] args){

        //System.out.println("\tToday I woke up @ 7:30.\nThen I went to the gym and did my daily exercise. \nLater, after having breakfest, I will need to study.");

        System.out.println("\tJava is a high-level, class-based," +
                "\n object-oriented programming language that is" +
                "\ndesigned to have as few implementation dependencies" +
                "\nas possible.\n\tJava was originally developed by" +
                "\nJames Gosling at Sun Microsystems. It was released" +
                "\nIn May 1995 as a core component of Sun Microsystems'\nJava platform." +
                "\n\tAs of March 2022, Java 18 is the latest version, while" +
                "\nJava 17, 11 and 8 are the current long-term support (LTS)\nversions. ");



    }
}
