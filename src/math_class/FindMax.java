package math_class;

public class FindMax {
    public static void main(String[] args) {
        int num1 = 10;
        int num2 = 15;

        int max = Math.max(num1,num2);
        System.out.println(max);

        int number1 = -25;
        int number2 = 35;
        int number3 = 5;
        int number4 = 18;

        System.out.println(Math.max(Math.max(number1, number2), Math.max(number3, number4)));

        number1 = -30;
        number2 = -40;
        number3 = 0;

        int max1 = Math.max(number1,number2);

        System.out.println(Math.max(max1, number3));




    }
}





