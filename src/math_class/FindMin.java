package math_class;

import java.util.Scanner;

public class FindMin {
    public static void main(String[] args) {
        Scanner inputReader = new Scanner(System.in);

        System.out.println("Please enter 2 numbers");
        int num1 = inputReader.nextInt();
        int num2 = inputReader.nextInt();

        System.out.println("the min of the given number is = " + Math.min(num1, num2));

        /* create a program to ask the user to enter 3 numbers
        print out the min of those 3 numbers
         */

        System.out.println("======Task-1=======");

        System.out.println("Please enter 3 numbers");
        int n1 = inputReader.nextInt();
        int n2 = inputReader.nextInt();
        int n3 = inputReader.nextInt();

        int n4 = Math.min(n1,n2);

        System.out.println("The min of " + n1 + " , " + n2 + " , " + n3 + " is: " + Math.min(n4,n3));






    }
}
