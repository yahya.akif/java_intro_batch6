package exceptions;

public class _01_Unchecked_Runtime_Exception {
    public static void main(String[] args) {

        String name = "John"; // 0123

        int [] numbers = {10,15,20};

       // System.out.println(name.charAt(5)); // StringIndexOutOfBoundException

       // System.out.println(numbers[5]); // ArrayIndexOutOfBoundException

        //String address = null;

        //System.out.println(address.toUpperCase());
        try{ // using try-catch to handle the exception
            System.out.println(name.charAt(5)); // StringIndexOutOfBoundsException
        }catch (StringIndexOutOfBoundsException e){
            System.out.println("I could not print char at 5 as it does not exist!");
        }

        System.out.println("End of the program");
    }
}
