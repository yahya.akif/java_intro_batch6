package exceptions.custom_exceptions;

public class Exercise01 {
    public static void main(String[] args) {
        System.out.println(checkAge(16)); // true
        System.out.println(checkAge(120)); // true
        System.out.println(checkAge(1)); // false
        System.out.println(checkAge(0)); // RuntimeException
        System.out.println(checkAge(-1)); // runtimeException
    }

    /*
    take int as argument and return true or false checkAge
    16 or more return true
    else less one or more 120 -> throw runtime exception
     */

    public static boolean checkAge(int age){
        if(age >= 16 && age <= 120) return true;
        else if (age > 0 && age < 16) return false;
        else throw new RuntimeException("The age cannot be less than 1 or more than 120!!!");
    }

    /*
    create methods to isCheckInHours()
    weak days 10-5pm
    end 10-3pm
     */

    public static boolean isCheckInHours(int day){
        if (day >= 1 && day <=7) return true;
        else throw new RuntimeException("The input does not represent anyday!!!");
    }
}
