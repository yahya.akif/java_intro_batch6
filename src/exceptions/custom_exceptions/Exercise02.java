package exceptions.custom_exceptions;

import utitlities.ScannerHelper;

public class Exercise02 {
    public static void main(String[] args) {
        /*
        Write a program that asks use to enter a number that represent the day of the week and check
        if it is valid to tell them it is in checkIn hours
         */

        int day = ScannerHelper.getNumber();

        try {
            if (Exercise01.isCheckInHours(day)) System.out.println("This is check in hours");
        }catch (Exception e){
            System.out.println("This is not check in hours!!!");
        }
    }
}
