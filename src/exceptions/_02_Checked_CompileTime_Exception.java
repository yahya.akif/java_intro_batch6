package exceptions;

public class _02_Checked_CompileTime_Exception {
    public static void main(String[] args) throws InterruptedException {

        for (int i = 0; i < 10; i++) {
            System.out.println(i);
            try {
                Thread.sleep(5000);
            }catch (InterruptedException e){
                System.out.println("I caught an exception here!!!");
            }
        }
    }
}
