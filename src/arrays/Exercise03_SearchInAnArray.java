package arrays;

import javax.crypto.spec.PSource;
import java.util.Arrays;

public class Exercise03_SearchInAnArray {
    public static void main(String[] args) {

        String[] objects = {"Remote", "Mouse", "Mouse", "Keyboard", "iPad"};

        /*
        check if it contains mouse = print true
         */

        boolean hasM = false;
        for (String object:objects) {
            if (object.equals("Remote"))
                hasM =true;
                break;
        }
        System.out.println(hasM);

        System.out.println("\n-------binary search-------");
        Arrays.sort(objects);

        System.out.println(Arrays.binarySearch(objects,"Mouse") >= 0);
        System.out.println(Arrays.binarySearch(objects,"Keyboard") >= 0);
        System.out.println(Arrays.binarySearch(objects,"mouse") >= 0); // upper and lower case affected
        System.out.println(Arrays.binarySearch(objects,"board") >= 0); // looking for exact match
    }

}
