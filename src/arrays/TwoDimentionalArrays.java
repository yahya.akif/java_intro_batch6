package arrays;

import java.util.Arrays;

public class TwoDimentionalArrays {
    public static void main(String[] args) {
        String[][] students = {
                {"Meerim", "Alina", "Carmela", "Ayat"},
                {"Yahya", "Adam", "Louie"},
                {"Dima", "Lesia", "Pinar"}
        };
        // print two-dimensional array
        System.out.println(Arrays.deepToString(students));

        // print an inner array
        System.out.println(Arrays.toString(students[0]));
        System.out.println(Arrays.toString(students[1]));
        System.out.println(Arrays.toString(students[2]));

        // how to print an element in an array
        System.out.println(students[1][2]);

        // How to print the length of two-dimensional array
        System.out.println(students.length);

        // how to print the length of each inner array
        System.out.println(students[0].length);
        System.out.println(students[1].length);
        System.out.println(students[2].length);

        //how to loop two dimentional array
        for (int i = 0; i < students.length; i++) {
            System.out.println(Arrays.toString(students[i]));
        }

        System.out.println("\n------2nd way----------\n");


        for (String[] innerArray : students) {
            System.out.println(Arrays.toString(innerArray));
        }

        System.out.println("\n----------loop each element-------\n");
        for (String[] inner : students) {
            for (String name : inner) {
                System.out.println(name);

            }

        }

    }
    }