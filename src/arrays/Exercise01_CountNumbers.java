package arrays;

import java.util.Arrays;

public class Exercise01_CountNumbers {
    public static void main(String[] args) {
        int[] numbers = {-1, 3, 0, 5, -7, 10, 8, 0, 10, 0};

        // write a program that counts how many negative numbers you in the array -> 3
        int negatives = 0;
        for (int number : numbers) {
              //  System.out.println(number);  all the numbers if needed
                if (number <0) negatives++;
            }
        System.out.println(negatives);


    int even = 0;
        for (int number : numbers) {
        //  System.out.println(number);  all the numbers if needed
        if (number %2 == 0) even ++;
    }
        System.out.println (even);

        System.out.println("\n--------sum-------\n");
        int sum =0;
        for (int num: numbers) {
                  System.out.println(sum += num);


        }

    }
}