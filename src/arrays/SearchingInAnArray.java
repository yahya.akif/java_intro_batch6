package arrays;

import java.util.Arrays;

public class SearchingInAnArray {
    public static void main(String[] args) {

        int[] numbers = {3,10,8,5,5};
        int count = 0;
        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i] == 7) count ++;
        }
        if (count >= 1) System.out.println(true);
        else System.out.println(false);

        System.out.println("\n---------Binary search-------");

        Arrays.sort(numbers); // WE need to sort to find a binary search

        System.out.println(Arrays.binarySearch(numbers,7)); // if it doesn't exist you will get a negative result
        System.out.println(Arrays.binarySearch(numbers,5)); // give you index of number
        System.out.println(Arrays.binarySearch(numbers,10)); // give you index of number
    }
}
