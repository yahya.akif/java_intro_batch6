package arrays;

import java.util.Arrays;

public class SortingArrays {
    public static void main(String[] args) {

        int[] numbers = {5, 3, 10};
        String[] names = {"Alex", "ali", "James", "John"};

        /*
        static - call it with class name

         */

        Arrays.sort(numbers);
        Arrays.sort(names);


        System.out.println(Arrays.toString(names));
        System.out.println(Arrays.toString(numbers));


    }
}