package arrays;

import java.lang.reflect.Array;
import java.util.Arrays;

public class UnderstandingArrays {
    public static void main(String[] args) {
        String[] cities = {"Chicago", "Miami", "Toronto"};
        System.out.println(cities.length);

        System.out.println(cities[1]); // Miami
        System.out.println(cities[0]); // Chicago
        System.out.println(cities[2]); // Toronto

        System.out.println(Arrays.toString(cities)); // convert array to string

        System.out.println("\n=========for loop=======\n");
        // how to loop an array
        for (int i = 0; i < 3; i++) {
            System.out.println(cities[i]);
        }
        for (int i = 0; i < cities.length; i++) {    // more dynamic
            System.out.println(cities[i]);
        }
        System.out.println("\n------------for each loop-------------\n");

        for(String city: cities) {
            System.out.println(city);
        }


        }

}
