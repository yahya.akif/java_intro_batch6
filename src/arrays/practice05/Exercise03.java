package arrays.practice05;

import utitlities.RandomGenerator;

import java.util.Arrays;

public class Exercise03 {
    public static void main(String[] args) {
        minAndMaxUsingSort();
        minAndMaxWithoutSort();

    }

    public static void minAndMaxUsingSort () {
              /*
        Write a program to generate 5 random numbers
between 1 to 10 (1 and 10 are included) and store those
numbers in an int array.
Find the max and min numbers among the random
numbers and print them.
Solve this question with sort and without sort.
         */
        int [] randArr = {RandomGenerator.getRandomNumber(1,10),
                RandomGenerator.getRandomNumber(1,10),
                RandomGenerator.getRandomNumber(1,10),
                RandomGenerator.getRandomNumber(1,10),
                RandomGenerator.getRandomNumber(1,10)};

        System.out.println(Arrays.toString(randArr));

        Arrays.sort(randArr);

        System.out.println("The max number is = " + randArr[0]);
        System.out.println("The min number is = " + randArr[4]);



    }

    public static void minAndMaxWithoutSort () {
        int [] randArr = {RandomGenerator.getRandomNumber(1,10),
                RandomGenerator.getRandomNumber(1,10),
                RandomGenerator.getRandomNumber(1,10),
                RandomGenerator.getRandomNumber(1,10),
                RandomGenerator.getRandomNumber(1,10)};

        System.out.println(Arrays.toString(randArr));

        int min = 10; // so we do not go into the negative
        int max = 1;

        for (int i:randArr) {
            if (i> max) max = i;
            else if (i<max) min = i;

            }
        System.out.println("The max number is = " + max);
        System.out.println("The min number is = " + min);

        }
    }


