package arrays.practice05;

public class Exercise04 {
    public static void main(String[] args) {
        String[] arr = {"banana", "Orange", "apple"};
        containsApple(arr);
    }

    public static void containsApple (String[] arr) {
        boolean hasApple = false;

        for (String s : arr) {
            if (s.equalsIgnoreCase("apple")) {
                hasApple = (true);
                break;
            }
        }

            System.out.println(hasApple);
        }

    }