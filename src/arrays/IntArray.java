package arrays;

import java.util.Arrays;

public class IntArray {
    public static void main(String[] args) {

        // Create an int array that will store 6 num
        int [] numbers = new int[6];

        System.out.println(Arrays.toString(numbers));

        numbers[0] = 5;
        numbers[2] = 15;
        numbers[4] = 25;

        System.out.println(Arrays.toString(numbers));

        for (int i = 0; i < numbers.length; i++) {
            System.out.println(i);
        }
        // print each element with for each loop
        for (int number:numbers) {
            System.out.println(number);
        }
    }
}
