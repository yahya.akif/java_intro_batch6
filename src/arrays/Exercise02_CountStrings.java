package arrays;

import java.util.Arrays;

public class Exercise02_CountStrings {
    public static void main(String[] args) {
        //1. declare a string array call as countries and assign a size of 3

        String [] countries = new String[3];

        //2. Assign "Spain" to index of 1
        countries [1] = "Spain";

        //3. print the value at index of 1 and 2

        System.out.println (countries[1]);
        System.out.println (countries[2]);

        //4. assign belgium at 0 and italy at 2

        countries [0] = "Belgium";
        countries [2] = "Italy";

        //5. print the array
        System.out.println(Arrays.toString(countries));


        //6. sort countries
        Arrays.sort(countries);

        System.out.println(Arrays.toString(countries));

        //7 loop an array

        for (int i = 0; i < countries.length; i++) {
            System.out.println(countries [i]);
        }

        //8. print each element with for each loop

        for (String country:countries
             ) {
            System.out.println(country);
        }

        //9. count how many countries have 5 characters ->2

        int count5 = 0;
        for (String country : countries) {
            if (country.length() == 5) count5++;
        }
        System.out.println(count5);

        //10. how many countries has letter I or i in their name
        int iCounter = 0;
        for (String country: countries
        ) {
           if (country.toLowerCase().contains("i")) iCounter++;
        }
        System.out.println(iCounter);
    }
}
