package practices;

import utitlities.ScannerHelper;

public class Exercise04_StringMethod {
    public static void main(String[] args) {
        String str = ScannerHelper.getString();

        if (str.length() < 2) {
            System.out.println("Length is less than 2");
    } else if (str.length() >= 2) {
            if (str.substring(0,2).equals(str.substring(str.length()-2))) { // cannot use == , Must use .equal
                System.out.println("True");
            } else System.out.println("False");
        } else System.out.println("False");

}}
