package practices;

import utitlities.ScannerHelper;

public class Exercise06_StringMethods {
    public static void main(String[] args) {
        String str = ScannerHelper.getString();

        if (str.length() < 4) {
            System.out.println("INVALID INPUT");
        } else {
            System.out.println(str.startsWith("xx") && str.endsWith("xx")); // ends with xx and starts with xx

        }
    }}