package practices;

import utitlities.ScannerHelper;

public class Exercise01_StringMethods {
    public static void main(String[] args) {

        System.out.println("\n=========Task-1========\n");

       String str = ScannerHelper.getString();

        System.out.println("The string given is = " + str);

        System.out.println("\n========Task-2==========\n");

        if (str.isEmpty()) {
            System.out.println("The string given is empty");
        } else System.out.println("The length is = " + str.length());

        System.out.println("\n========Task-3==========\n");

        if (str.isEmpty()) {
            System.out.println("There is no character in the sString");
        }else System.out.println(str.charAt(0));

        System.out.println("\n=========Task-4========\n");

        if (!str.isEmpty()) {
            System.out.println("The last character is = " + str.charAt(str.length() - 1));
        } else System.out.println("There is no character in this String");

        System.out.println("\n========Task-5===========\n");

        if (str.toLowerCase().contains("a") || str.toLowerCase().contains("e") || str.toLowerCase().contains("i") ||
                str.toLowerCase().contains("o") || str.toLowerCase().contains("u")) {
            System.out.println("This String has a vowel");
        } else System.out.println("This String does not have a vowel");
        }


    }
