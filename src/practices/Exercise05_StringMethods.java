package practices;

import utitlities.ScannerHelper;

public class Exercise05_StringMethods {
    public static void main(String[] args) {
        String str = ScannerHelper.getString();
        String str2 = ScannerHelper.getString();
        if (str.length() < 2 || str2.length() < 2) {
            System.out.println("INVALID INPUT");
        }
        else System.out.println(str.substring(1, str.length()-1) + str2.substring(1,str.length()-1));
    }

}
