package regex;

import utitlities.ScannerHelper;


import java.util.Scanner;
import java.util.regex.Pattern;

public class ValidUsername {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Please enter a username");
        String input = scanner.nextLine();
        //String regex = ("[a-zA-Z0-9]{5,10}");
        if (Pattern.matches("[a-zA-Z0-9]{5,10}",input)) System.out.println("Valid Username");
        else System.out.println("Error! Username must be 5 to 10 characters long");
    }




}
