package regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CountWords {
    public static void main(String[] args) {

        Pattern pattern = Pattern.compile ("[A-Za-z]{2,}");
        Matcher matcher = pattern.matcher("hello, my name is john.");

        int counter = 0;
        while (matcher.find()) {
            System.out.println(matcher.group()); // prints out what you find

            counter++;
        }
            System.out.println("The sentence contains " + counter + " words.");
        }

       // [A-Za-z0-9_-]{9,15}


}
