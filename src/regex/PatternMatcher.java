package regex;

import utitlities.ScannerHelper;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PatternMatcher {
    public static void main(String[] args) {
        // String input = ScannerHelper.getString();
        // System.out.println(Pattern.matches("[a-z0-9_-]{3,10}",input));
        // "java" --> true
        // "i love java --> false
        // "1a2B3" -> false
        // "tech-global-school -> false

        Pattern pattern = Pattern.compile("Java"); // compiles String regex into a pattern "[a-z0-9_ -]{3,}"
        Matcher matcher = pattern.matcher("I Love Java, Java is fun");
        System.out.println(pattern);  // prints out compiles regex pattern as a pattern

        // System.out.println(pattern.toString()); // prints out compiles regex pattern as a String
        // System.out.println(pattern.pattern()); // prints out compiled regex pattern as a string

        System.out.println(matcher.matches());





//        while (matcher.find()){
//            System.out.println(matcher.group());
//        }

        int counter = 0;
        while (matcher.find()) {  // find next matched word in a string
            counter++;
            System.out.println(matcher.group()); // print matched word
            System.out.println(matcher.start()); // starting index of matched word
            System.out.println(matcher.end()); // ending index of matched word
        }
        System.out.println("Java count : " + counter);
    }
}
