package string_Methods;

public class _01_ValueOf_Method {
    public static void main(String[] args) {
        /*
            1- return type
            2- String - class name
            3- static - using class String to call it
            4-takes any variable as an argument
         */
        int num = 125;

        String numAsStr= String.valueOf(num);

        System.out.println(num+5); // still is an int
        System.out.println(numAsStr+5); // Made the int into a String

        char c = 'B';

        System.out.println(c);
        System.out.println(String.valueOf(c).toLowerCase());
    }
}
