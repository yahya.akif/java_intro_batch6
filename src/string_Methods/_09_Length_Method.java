package string_Methods;

public class _09_Length_Method {
    public static void main(String[] args) {
        /*
        1- return type
        2- return an int
        3- non-static
        4- no arguments taken
         */

        String str = "TechGlobal";
        String str2 = "I am learning java and it is fun";

        int lengthOfStr = str.length();

        System.out.println(lengthOfStr);
        System.out.println(str2.length());
    }
}
