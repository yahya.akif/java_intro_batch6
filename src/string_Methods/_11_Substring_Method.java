package string_Methods;

public class _11_Substring_Method {
    public static void main(String[] args) {
        /*
        1- return
        2- return string
        3- non-static
        4- yes taking int and putting out string or char
         */

        String str = "I love java a lot";

        String firstWord = str.substring(0,1); // find I
        String secondWord = str.substring(2,6); // find love
        //String thirdWord = str.substring(7,11); // find love

        String thirdWord = str.substring(7); // find love

        System.out.println(firstWord);
        System.out.println(secondWord);
        System.out.println(thirdWord);

        System.out.println(str.substring(str.length()/2));
    }
}
