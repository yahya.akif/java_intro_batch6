package string_Methods;

public class Exercise03 {
    public static void main(String[] args) {

        String str = "The best is Java";

        String java = str.substring(12);
        System.out.println(java);

        // second way

        java = str.substring(str.indexOf("Java"));
        System.out.println(java);

        // third way

        java = str.substring(str.lastIndexOf(' ')).trim();
        System.out.println(java);
    }
}
