package string_Methods;

import java.util.Scanner;

public class Exercise02 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("Please enter your favourite book?");
        String book = input.nextLine();

        System.out.println("Please enter your favourite quote from the book");
        String quote = input.nextLine();

        System.out.println("My favourite book is " + book +
                ", and my favourite quote is\"" + quote + "\".");
    }
}
