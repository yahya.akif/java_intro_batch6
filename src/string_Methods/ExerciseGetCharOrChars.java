package string_Methods;

public class ExerciseGetCharOrChars {
    public static void main(String[] args) {
       String name1 = "bilal";
       String name2 = "Matthew";
       String name3 = "Ronaldo!!";
       String name4 = "Okan";
       String name5 = "yousef";

       // to find middle character of an odd word we would need to do (length()-1/2

        System.out.println(name1.charAt((name1.length()-1)/2)); // l
        System.out.println(name2.charAt((name2.length()-1)/2)); // t
        System.out.println(name3.charAt((name3.length()-1)/2)); // l

        System.out.println("" + name4.charAt((name4.length())/2-1) + name4.charAt(name4.length()/2));
        System.out.println("" + name5.charAt((name5.length()/2-1)) + name5.charAt(name5.length()/2));


    }

}
