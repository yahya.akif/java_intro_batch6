package string_Methods;

public class _08_IndexOf_Method {
    public static void main(String[] args) {

        /*
        1- return
        2- returns an int
        3- non-static
        4- takes a string or char as an argument
         */
        String str = "TechGlobal";
        System.out.println(str.indexOf("h"));

        System.out.println(str.indexOf('T')); // -1
        System.out.println(str.indexOf('x'));

        System.out.println(str.indexOf("Tech")); // 0 - first letter

        System.out.println(str.indexOf("Global")); // 4
        System.out.println(str.indexOf("global")); // -1 it is case sensitive

        // lastIndexOf
        System.out.println(str.lastIndexOf('l'));
    }
}
