package string_Methods;

public class _15_IsEmpty {
    public static void main(String[] args) {

        /*
        1- return type
        2- returns boolean
        3- non-static
        4- does not take arguments
         */
        String emptyStr = "";
        String word = "Hello";

        System.out.println("first String is empty = " + emptyStr.isEmpty());
        System.out.println("first String is empty = " + word.isEmpty());
    }
}
