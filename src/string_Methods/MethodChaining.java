package string_Methods;

public class MethodChaining {
    public static void main(String[] args) {
        String str = "TechGlobal";

        //  single method
        System.out.println(str.toLowerCase());

        //2 methods chaining
        System.out.println(str.toLowerCase().contains("tech")); // since last part is boolean can no longer chain

        //3 methods chained
        System.out.println(str.toUpperCase().substring(4).length());

        //6 methods chained
        String sentence = "Hello, my name is john doe. I am 30 and I go to school at tech global";
        System.out.println(sentence.toLowerCase().replace("a", "x")
                .replace("e", "x").replace("i","x")
                .replace("o","x").replace("u","x"));
    }
}
