package string_Methods;

import java.util.Scanner;

public class Exercise05 {
    public static void main(String[] args) {
        /*

         */

        Scanner input = new Scanner(System.in);
        System.out.println("Please enter a line of text");
        String word = input.nextLine();

        if (word.startsWith("a") && word.endsWith("e")) {
            System.out.println("true");
        } else {
            System.out.println("false");
        }
    }}
