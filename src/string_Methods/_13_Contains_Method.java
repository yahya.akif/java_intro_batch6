package string_Methods;

import utitlities.ScannerHelper;

import java.util.Scanner;

public class _13_Contains_Method {
    public static void main(String[] args) {
        /*
        1- return
        2- returns boolean
        3- non-static
        4- take argument char or string
         */

        String name = "John Doe";

        boolean containsJohn = name.contains("John");
        System.out.println(containsJohn);

        // write a program to ask user to enter string return true if it is a sentence &
        // false if it is a single word

        String str = ScannerHelper.getString();
        System.out.println(str.contains(" ") && str.endsWith("."));
    }
}
