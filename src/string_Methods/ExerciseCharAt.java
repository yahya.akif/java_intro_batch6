package string_Methods;

import java.util.Scanner;

public class ExerciseCharAt {
    public static void main(String[] args) {
        String str = "TechGlobal";
        String str2 = "Hello World";
        String str3 = "I really love Java";
        System.out.println(str.charAt(4)); // G

        System.out.println(str.charAt(9));
        System.out.println(str2.charAt(9));
        System.out.println(str3.charAt(9));

        //2nd way!!!

        System.out.println(str.charAt(str.length()-1));
        System.out.println(str2.charAt(str2.length()-1));
        System.out.println(str3.charAt(str3.length()-1));

        // Task ask user to enter a string and print our the last character of that string
        Scanner input = new Scanner(System.in);
        String s = input.nextLine();

        System.out.println(str.charAt(str.length()-1));
    }
}
