package string_Methods;

import java.util.Arrays;

public class _16_toCharArray_Method {
    public static void main(String[] args) {

        String name = "john";

        char[] charOfName = name.toCharArray();

        System.out.println(Arrays.toString(charOfName));
        System.out.println(charOfName.length);
    }
}
