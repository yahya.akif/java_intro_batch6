package string_Methods;

import java.util.Scanner;

public class Exercise01 {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        System.out.println("Please enter 2 strings");
        String str1 = input.nextLine();
        String str2 = input.nextLine();

        System.out.println("Please enter 2 strings");
        String str3 = input.nextLine();
        String str4 = input.nextLine();

        //String str1 = "Java is fun";
        //String str2 = "Hello world";
        //String str3 = "Hello";
        //String str4 = "Hello";
        if (str1.equals(str2)) {
            System.out.println("These things are equal");
        } else {
            System.out.println("These things are not equal");
        }
        System.out.println(str1.equals(str2)); // false
        System.out.println(str3.equals(str4)); // true
    }
}
