package string_Methods;

public class _03_Equals_Method {
    public static void main(String[] args) {

        /*
        1. returns boolean
        2. non-static
        3. takes an objects as an argument (in this case takes a String)
         */

        String str1 = "Tech";
        String str2 = "Global";
        String str3 = "tech";
        String str4 = "TechGlobal";
        String str5 = "Tech";

        Boolean isEquals = str1.equals(str2);


        System.out.println(isEquals); // false
        System.out.println(str1.equals(str3)); // false

        System.out.println((str1.concat(str2)).equals(str4)); // true

        System.out.println(!str1.equals(str5)); // false
        System.out.println(!str1.equals(str4)); // true
    }
}
