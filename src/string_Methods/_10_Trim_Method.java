package string_Methods;

public class _10_Trim_Method {
    public static void main(String[] args) {

        /*
        1- return type
        2- returns a string
        3- non-static
        4- no arguments
         */
        String str = "     TechGlobal   ";

        System.out.println(str);
        System.out.println(str.trim());

        String str2 = " hel  lo wor  ld  ";
        System.out.println(str2);
        System.out.println(str2.trim()); // hel lo wor ld
    }
}
