package string_Methods;

public class exercise04 {
    public static void main(String[] args) {
        /* assume you have a string
        "I go to TechGlobal"
         */
        String sentence =  "I go to TechGlobal";
        System.out.println(sentence.substring(0,1));
        System.out.println(sentence.substring(2,4));
        System.out.println(sentence.substring(5,7));
        System.out.println(sentence.substring(8));
    }
}
