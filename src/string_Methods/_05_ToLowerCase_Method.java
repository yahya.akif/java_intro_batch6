package string_Methods;

public class _05_ToLowerCase_Method {
    public static void main(String[] args) {

        /*
        1. return type
        2. returns a string
        3. non-static
        4. does not take argument
         */
        String str1 = "JaVA IS FUN";

        System.out.println(str1);
        System.out.println(str1.toLowerCase()); // all lower case

        char c = 'A';
        System.out.println(String.valueOf(c)); // convert it to String
        System.out.println(String.valueOf(c).toLowerCase()); // use method chaining
    }
}

