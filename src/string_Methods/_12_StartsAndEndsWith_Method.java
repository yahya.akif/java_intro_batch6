package string_Methods;

public class _12_StartsAndEndsWith_Method {
    public static void main(String[] args) {
        /*
        1- return
        2- returns boolean
        3- non-static
        4- yes Takes a char or String as arguments
         */

        String str = "TechGlobal";

        boolean startsWith = str.startsWith("T");
        boolean endsWith = str.endsWith("T");

        System.out.println(startsWith); // True
        System.out.println(endsWith);  // false


    }
}
