package string_Methods;

public class _07_CharAt_Method {
    public static void main(String[] args) {
        /*
        1- return type
        2- return char
        3- non-static
        4- it takes an int index as an argument
         */

        String name = "Bilal";

        char firstLetter = name.charAt(0); // B
        char secondLetter = name.charAt(1); // B
        char thirdLetter = name.charAt(2); // B
        char fourthLetter = name.charAt(3); // B
        System.out.println(name);
        System.out.println(firstLetter);
        System.out.println(secondLetter);
        System.out.println(thirdLetter);
        System.out.println(fourthLetter);

        char fifthLetter = name.charAt(4); // we always go index-1
        System.out.println(fifthLetter);

    }
}
