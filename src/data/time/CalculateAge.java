package data.time;

import java.time.LocalDate;
import java.util.Scanner;

public class CalculateAge {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        System.out.println("Please enter your year of birth?");
        int age = input.nextInt();
        LocalDate date = LocalDate.now();
        System.out.println(date.getYear()-age);
    }
}
