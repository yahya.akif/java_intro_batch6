package data.time;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

public class _01_LocalDateTime {
    public static void main(String[] args) {
        System.out.println("------LocalDate");
        LocalDate currentDate = LocalDate.now();
        System.out.println(currentDate); // yyyy-MM-dd

        System.out.println(currentDate.getYear());
        System.out.println(currentDate.getDayOfYear());



        System.out.println(currentDate.format(DateTimeFormatter.ofPattern("MM/dd/yyyy")));
        System.out.println(currentDate.format(DateTimeFormatter.ofPattern("yyyy")));
        System.out.println(currentDate.plusYears(2)); // jump to 2025
        System.out.println(currentDate.plusMonths(5)); //add 5 months
        System.out.println(currentDate.minusDays(1).format(DateTimeFormatter.ofPattern("MMM d,yyyy"))); //month name day, year

        System.out.println("----localTime-----");
        LocalTime localTime = LocalTime.now();
        System.out.println(localTime); //hh:mm:ss.SSS

        System.out.println("\n--LocalDateTime-----\n");
        LocalDateTime currentDateTime = LocalDateTime.now();

        System.out.println(currentDateTime); //yyyy-MM--ddThh:mm:ss.SSS
        System.out.println(currentDateTime.format(DateTimeFormatter.ofPattern("MMM d,yyyy h:mm a"))); //May 2, 2023 6:43 PM

        System.out.println(ChronoUnit.DAYS.between(LocalDate.of(2023, Month.JANUARY, 23), LocalDate.now()));
        System.out.println(ChronoUnit.DAYS.between(LocalDate.of(1993, Month.JANUARY, 1), LocalDate.now()));
        System.out.println(ChronoUnit.YEARS.between(LocalDate.of(1993, Month.JANUARY, 1), LocalDate.now()));
        System.out.println(ChronoUnit.MONTHS.between(LocalDate.of(1993, Month.JANUARY, 1), LocalDate.now()));
    }
}
